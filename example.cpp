


#include <iostream>
#include <iomanip>
#include "laft.h"

void print_title(const char*);

using namespace std;
using namespace laft;

void print_title(const char*str){
  cout<<endl<<endl;
  char prev= cout.fill('*');
  cout<<'/'<<setw(79)<<'*'<<endl;
  cout<<" *"<<endl;
  cout<<" *  TEST: "<<str<<endl;
  cout<<" *"<<endl;
  cout<<' '<<setw(78)<<'*'<<'/'<<endl;
  cout.fill(prev);
}

int main(int argc,char*argv[]){
  
  
  
  print_title("Creation of vector");
  try{
    Vector v1(5,COLUMN_VECTOR);
    cout<<v1;
    
    Vector v2(5,ROW_VECTOR);
    cout<<v2;
    
    Vector v3(3,ONES,COLUMN_VECTOR);
    cout<<v3;
    
    Vector v4(3,ONES,ROW_VECTOR);
    cout<<v4;
    
    double a[5]= {1.0,2.0,3.0,4.0,5.0};
    Vector v5(3,a);
    cout<<v5;
    
    Vector v6(5,a,ROW_VECTOR);
    cout<<v6;
    
    Vector v7("1, 3, 5, 7, 9");
    cout<<v7;
    
    Vector v8("1; 3; 5; 7; 9");
    cout<<v8;
    
    Vector v9(10,RAND);
    cout<<"A random vector of which the elements are uniformly distributed"<<endl;
    cout<<v9;
    
    Vector v10(10,RANDN);
    cout<<"Another random vector of which the elements are normally distributed"<<endl;
    cout<<v10;
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  print_title("Index, assignment, comparison operators");
  try{
    Vector v1("1; 2; 3; 4; 5");
    cout<<v1;
    
    cout<<"The elements of v1 are "<<endl;
    cout<<"v1(1) = "<<v1(1)<<",  "<<"v1(2) = "<<v1(2)<<",  "
    <<"v1(3) = "<<v1(3)<<",  "<<"v1(4) = "<<v1(4)<<",  "
    <<"v1(5) = "<<v1(5)<<endl;
    
    v1(2)= -v1(2);
    v1(4)= -v1(4);
    cout<<v1;
    
    
    Vector v2("1; 3; 5");
    Vector v3("1 3 5");
    
    if(v2==v3){
      cout<<"They are the same."<<endl;
    }else{
      cout<<"Row and column vectors are different."<<endl;
    }
    
    if(v2==v2){
      cout<<"They are the same object!"<<endl;
    }else{
      cout<<"There is some bug in this library."<<endl;
    }
    
    Vector v4= v2;
    if(v2!=v4){
      cout<<"There is some bug in this library."<<endl;
    }else{
      cout<<"They are the same."<<endl;
    }
    
    Vector v5("1; 2; 3; 4; 5");
    Vector v6("1; 2; 3; 4; 5.000000000000022205");
    
    cout<<"Tolerance of Vector, v5 is "<<v5.tolerance()<<endl;
    for(unsigned i= 0;i!=5;i++){
      v5.increaseTolerance();
      
      cout<<"Modified tolerance of Vector, v5 is "
      <<v5.tolerance()
      <<" and current tolerance level is "
      <<v5.toleranceLevel()
      <<endl;
      
      if(v5==v6){
        cout<<"They are different, but examined to be equal."<<endl;
      }else if(v5.isSimilarTo(v6)){
        cout<<"They are different, but examined to be similar."<<endl;
      }else{
        cout<<"They are different, and examined to be different, too."<<endl;
      }
    }
    
    v5.initTolerance();
    cout<<"Modified tolerance of Vector, v5 is "
    <<v5.tolerance()
    <<" and current tolerance level is "
    <<v5.toleranceLevel()
    <<endl;
    
    if(v5==v6){
      cout<<"They are different, but examined to be equal."<<endl;
    }else if(v5.isSimilarTo(v6)){
      cout<<"They are different, but examined to be similar."<<endl;
    }else{
      cout<<"They are different, and examined to be different, too."<<endl;
    }
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  
  print_title("Addition/subtraction of vectors");
  try{
    Vector v1("1, 3, 5, 7, 9");
    Vector v2(5,ONES,ROW_VECTOR);
    
    Vector v3= v1+v2;
    cout<<v3;
    
    Vector v4= v1-v2;
    cout<<v4;
    
    Vector v5= -v2;
    cout<<v5;
    
    cout<<v1+1;
    cout<<1+v1;
    cout<<v1-1;
    cout<<1-v1;
    
    cout<<(v1+= 3);
    cout<<(v1-= 3);
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  print_title("Multiplication and division of vectors");
  try{
    Vector v1("1, 3, 5");
    cout<<v1;
    
    Vector v2= v1*2.0;
    cout<<v2;
    
    v2*= 2.0;
    cout<<v2;
    
    Vector v3= v1/2.0;
    cout<<v3;
    
    v3/= 2.0;
    cout<<v3;
    
    Vector v4= 0.5*v1*3.0/2.0;
    cout<<v4;
    
    Matrix m1("1 2 3; 4 5 6; 7 8 9");
    Vector v5= v1*m1;
    cout<<v5;
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  
  
  
  print_title("Insert/append/delete element(s)");
  try{
    Vector v0;
    v0.appendElement(1.0);
    cout<<v0;
    v0.removeElementAt(1);
    cout<<v0;
    v0.insertElementAt(1.0,1);
    cout<<v0;
    
    Vector v1("1, 3, 5");
    cout<<v1;
    
    cout<<"> insertElementAt()"<<endl;
    v1.insertElementAt(2.0,2);
    cout<<v1;
    
    v1.insertElementAt(4.0,4);
    cout<<v1;
    
    cout<<"> appendElement()"<<endl;
    v1.appendElement(6.0);
    cout<<v1;
    
    Vector v2("-1, -3, -5");
    cout<<"> insertVectorAt()"<<endl;
    v1.insertVectorAt(v2,1);
    cout<<v1;
    
    v1.insertVectorAt(v2,7);
    cout<<v1;
    
    cout<<"> appendVector()"<<endl;
    v1.appendVector(v2);
    cout<<v1;
    
    cout<<"> removeElementsInRange()"<<endl;
    v1.removeElementsInRange(1,5);
    cout<<v1;
    
    cout<<"> removeElementAt() "<<endl;
    laft::Size count= v1.dim();
    for(unsigned i= 0;i!=count;i++){
      v1.removeElementAt(1);
      cout<<v1;
    }
    
    cout<<"> insertElementAt() "<<endl;
    for(unsigned i= 1;i!=5;i++){
      v1.insertElementAt((double)i,1);
      cout<<v1;
    }
    
    cout<<"> removeElementsInRange() "<<endl;
    v1.removeElementsInRange(1,v1.dim());
    cout<<v1;
    
    cout<<"> insertVectorAt() "<<endl;
    v1.insertVectorAt(v2,1);
    cout<<v1;
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  print_title("p-norm, transpose, and inner product of vector(S)");
  try{
    Vector v1("1; 3; 5");
    cout<<"1-norm of v1 is "<<v1.norm(1)<<endl;
    cout<<"2-norm of v1 is "<<v1.norm(2)<<endl;
    cout<<"Infinity norm of v1 is "<<v1.norm(Inf)<<endl;
    Vector v2= unit(v1);
    cout<<"Unit vector whose direction is parallel to v1 is "
    <<v2<<endl;
    
    cout<<"Transpose of v1, transpose(v1), is "<<transpose(v1)
    <<endl;
    
    cout<<"v1'v1 is "<<transpose(v1)*v1<<endl;
    
    Vector v3("2 4 6");
    cout<<"Inner product of two vectors, dot(v1,v3), is "<<dot(v1,v3)<<endl;
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  print_title("Creation of matrix");
  try{
    Matrix m1(3,3);
    cout<<m1;
    
    Matrix m2(2,4,ONES);
    cout<<m2;
    
    double v[12]= {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0};
    Matrix m3(3,4,v);
    cout<<m3;
    
    Matrix m4("1, 2, 3; 4, 5, 6; 7, 8, 9; 10, 11, 12");
    cout<<m4;
    
    Matrix m5(4,4,RAND);
    cout<<"Uniformly random matrix"<<endl;
    cout<<m5;
    
    Matrix m6(4,4,RANDN);
    cout<<"Normally random matrix"<<endl;
    cout<<m6;
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  print_title("Index, assignment, comparison operators of matrix");
  try{
    Matrix m1("1 2 3; 4 5 6; 7 8 9");
    cout<<m1;
    
    cout<<"The element at (1,1) is "<<m1(1,1)<<endl;
    cout<<"The element at (2,3) is "<<m1(2,3)<<endl;
    
    Matrix m2("1 2 3; 4 5 6");
    Matrix m3= m2;
    if(m2==m3){
      cout<<"They are the same."<<endl;
      cout<<m2;
      cout<<m3;
    }else{
      cout<<"Something wrong."<<endl;
      cout<<m2;
      cout<<m3;
    }
    
    cout<<"Changing the element at (2,3) to 0."<<endl;
    m2(2,3)= 0.0;
    if(m2!=m3){
      cout<<"They are different."<<endl;
      cout<<m2;
      cout<<m3;
    }else{
      cout<<"Something wrong."<<endl;
      cout<<m2;
      cout<<m3;
    }
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  print_title("Addition/subtraction of matrices");
  try{
    Matrix m1(3,3,ONES);
    Matrix m2("1 2 3; 4 5 6; 7 8 9");
    
    cout<<m1;
    cout<<m2;
    
    cout<<"The addition of two matrices:";
    cout<<m1+m2;
    
    cout<<"The subtraction of two matrices:";
    cout<<m2-m1;
    
    cout<<"Addition of a matrix and a scalar:";
    cout<<m1+1;
    cout<<1+m1;
    
    cout<<"Subtraction of a matrix and a scalar:";
    cout<<m2-1;
    cout<<1-m2;
    
    cout<<"+=, -= operators:";
    cout<<(m1+= m2);
    cout<<(m1-= m2);
    
    cout<<"Negation operator:";
    cout<<-m2;
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  print_title("Manipulation of matrix");
  try{
    Matrix m1;
    Vector v1(5,ZEROS);
    Vector v2(5,RANDN);
    m1.appendColumnVector(v1);
    m1.appendColumnVector(v2);
    cout<<m1;
    
    m1.appendColumns(m1);
    cout<<m1;
    
    Vector v3(m1.numRows(),ONES);
    m1.insertColumnVectorAt(v3,1);
    cout<<m1;
    
    m1.insertColumnVectorAt(v3,3);
    cout<<m1;
    
    m1.insertColumnVectorAt(v3,m1.numCols());
    cout<<m1;
    
    unsigned imax= m1.numCols();
    for(unsigned i= 0;i!=imax;i++){
      m1.deleteColumnAt(1);
      cout<<m1;
    }
    
    Matrix m2;
    m2.appendRowVector(v1.T());
    m2.appendRowVector(v2.T());
    cout<<m2;
    
    m2.appendRows(m2);
    cout<<m2;
    
    Vector v4(m2.numCols(),ONES,ROW_VECTOR);
    m2.insertRowVectorAt(v4,1);
    cout<<m2;
    
    m2.insertRowVectorAt(v4,3);
    cout<<m2;
    
    m2.insertRowVectorAt(v4,m2.numRows());
    cout<<m2;
    
    imax= m2.numRows();
    for(unsigned i= 0;i!=imax;i++){
      m2.deleteRowAt(1);
      cout<<m2;
    }
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  
  
  
  print_title("Transpose of matrix");
  try{
    Matrix m1("1 2 3 4; 5 6 7 8; 9 10 11 12");
    Matrix m2= m1.transpose();
    Matrix m3= transpose(m1);
    cout<<m1;
    cout<<m2;
    cout<<m3;
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  print_title("Inverse of matrix");
  try{
    Matrix m1("1 2; -2 1");
    cout<<m1;
    cout<<"The inverse of the matrix above is "<<m1.inv();
    
    Matrix m2("-4 1; 5 -1");
    cout<<m2;
    cout<<"The multiplication of the above matrix and its inverse is "<<
    m2*m2.inv();
    
    Matrix m3("7 5 9; 2 1 5; 1 1 0");
    cout<<m3;
    cout<<"The inverse of the matrix above is "<<inv(m3);
    
    Matrix m4("1 1 0; 1 0 1; 0 1 1");
    cout<<m4;
    cout<<"The inverse of the matrix above is "<<inv(m4);
    cout<<"The multiplication of the two matrices above is "<<m4*inv(m4);
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  print_title("Trace of matrix");
  try{
    Matrix m1= ("1 2 3; 4 5 6; 7 8 9");
    cout<<"Trace of the following matrix is "<<trace(m1);
    cout<<m1;
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  
  print_title("Determinant of matrix");
  try{
    Matrix m1= eye(3);
    cout<<"Determinant of 3 x 3 identity matrix is "<<m1.det()<<endl;
    
    Matrix m2("1 2; -2 1");
    cout<<"Determinant of the following matrix is "<<m2.det()<<endl;
    cout<<m2;
    
    Matrix m3("-4 1; 5 -1");
    cout<<"Determinant of the following matrix is "<<m3.det()<<endl;
    cout<<m3;
    
    Matrix m4("1 -1 1; 2 -2 2; 3 -3 3");
    cout<<"Determinant of the following matrix is "<<det(m4)<<endl;
    cout<<m4;
    
    Matrix m5("1 -2 3; -2 4 6; 1 2 3");
    cout<<"Determinant of the following matrix is "<<det(m5)<<endl;
    cout<<m5;
    
    Matrix m6("7 5 9; 2 1 5; 1 1 0");
    cout<<"Determinant of the following matrix is "<<det(m6)<<endl;
    cout<<m6;
    
    Matrix m7("1 1 0; 1 0 1; 0 1 1");
    cout<<"Determinant of the following matrix is "<<det(m7)<<endl;
    cout<<m7;
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  
  
  
  print_title("Singular value decomposition");
  try{
    Matrix m1("7.52,-1.10,-7.95,1.08;-0.76,0.62,9.34,-7.10;\
              5.13,6.62,-5.66,0.87;-4.75,8.52,5.75,5.30;\
              1.33,4.91,-5.49,-3.52;-2.40,-6.77,2.34,3.95");
    
    Matrix u1;
    Matrix v1;
    Vector s= m1.svd(u1,v1);
    cout<<"Original matrix"<<endl;
    cout<<m1;
    cout<<"Singular values:"<<endl;
    cout<<s;
    cout<<"Left singular vectors:"<<endl;
    cout<<u1;
    cout<<"Right singular vectors:"<<endl;
    cout<<v1;
    
    cout<<"Singular values using Matlab-like function:"<<endl;
    cout<<svd(m1);
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  print_title("Eigenvalues & Eigenvectors");
  try{
    Matrix m1("1, 2, 3, 4; 2, 2, 3, 4; 3, 3, 3, 4; 4, 4, 4, 4");
    cout<<m1;
    
    Matrix ev1= eigenvalues(m1);
    ev1.printMatrixAsEigenvalueEigenvector();
    
    Matrix m2("0, 2, 0, 1; 2, 2, 3, 2; 4, -3, 0, 1; 6, 1, -6, -5");
    cout<<m2;
    
    Matrix ev2= m2.eigenvectors();
    ev2.printMatrixAsEigenvalueEigenvector();
    
    Matrix ev3= leftRightEigenvectors(m2);
    printMatrixAsEigenvalueEigenvector(ev3);
    
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  print_title("Matlab-like matrix generators");
  try{
    Matrix m1= eye(3);
    cout<<m1;
    
    Matrix m2= eye(3,5);
    cout<<m2;
    
    Matrix m3= zeros(3);
    cout<<m3;
    
    Matrix m4= ones(4,3);
    cout<<m4;
    
    Matrix m5= diag(Vector("1 3 5"));
    cout<<m5;
    
    Matrix m6= diag(Vector("2 4 6"),2);
    cout<<m6;
    
    Matrix m7= diag(Vector("-2 -4 -6"),-3);
    cout<<m7;
  }catch(error::Generic err){
    cout<<err.message();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  return 0;
}









