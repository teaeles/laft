


#include "laft.h"

using namespace laft;



error::Generic::Generic(void):theMessage("unknown"){}
error::Generic::Generic(const char*msg):theMessage(msg){}
error::Generic::~Generic(void){}
const std::string&error::Generic::message(void)const{
  return theMessage;
}



error::NotPermitted::NotPermitted()
:Generic("ERROR: The operation is not permitted."){}
error::Singular::Singular()
:Generic("ERROR: Matrix is singular to working precision."){}
error::FileError::FileError()
:Generic("ERROR: Generic file error."){}
error::NonSquare::NonSquare()
:Generic("ERROR: Matrix should be square."){}
error::NonPositiveDefinite::NonPositiveDefinite()
:Generic("ERROR: Matrix should be positive definite."){}
error::NonPositiveSemiDefinite::NonPositiveSemiDefinite()
:Generic("ERROR: Matrix should be positive semi-definite."){}
error::NotImplemented::NotImplemented()
:Generic("ERROR: Feature not implemented yet."){}
error::RankDefficient::RankDefficient()
:Generic("ERROR: Matrix is rank defficient."){}
error::CplusplusElevenRequired::CplusplusElevenRequired()
:Generic("ERROR: C++11 or higher is required."){}














error::DivisionByZero::DivisionByZero()
:Generic("ERROR: Division by zero."){}






error::IllegalConstruction::IllegalConstruction()
:Generic("ERROR: Illegal argument in constructor."){}








error::OutOfRangeIndex::OutOfRangeIndex()
:Generic("ERROR: The index is out of range."){}










error::DimensionMismatch::DimensionMismatch()
:Generic("ERROR: Dimensions mismatched in operation."){}








error::FailedToConverge::FailedToConverge()
:Generic("ERROR: The algorithm failed to converge."){}







error::FailureInSolvingEigenvalues::FailureInSolvingEigenvalues()
:Generic("ERROR: Solving eigenvalues failed."){}















void
laft::_copy_elements(laft::Size sz,DBL*src,DBL*des){
  for(Index i= 0;i!=sz;i++){
    des[i]= src[i];
  }
}




void
laft::_add_elements(laft::Size sz,DBL*src1,DBL*src2,DBL*des){
  for(Index i= 0;i!=sz;i++){
    des[i]= src1[i]+src2[i];
  }
}

void
laft::_subtract_elements(laft::Size sz,DBL*src1,DBL*src2,DBL*des){
  for(Index i= 0;i!=sz;i++){
    des[i]= src1[i]-src2[i];
  }
}




void
laft::_negate_elements(laft::Size sz,DBL*src,DBL*des){
  for(Index i= 0;i!=sz;i++){
    des[i]= -1.0*src[i];
  }
}




void
laft::_multiply_scalar_to_elements(laft::Size sz,DBL*src,const DBL&s,DBL*des){
  for(Index i= 0;i!=sz;i++){
    des[i]= src[i]*s;
  }
}

void
laft::_divide_elements_by_scalar(laft::Size sz,DBL*src,const DBL&s,DBL*des){
  if(std::fabs(s)<EPS){
    throw error::DivisionByZero();
  }
  for(Index i= 0;i!=sz;i++){
    des[i]= src[i]/s;
  }
}




bool
laft::_exclusiveOR(const bool&a,const bool&b){
  return((a&&!b)||(!a&&b));
}




laft::Size
laft::_min(const laft::Size&a,const laft::Size&b){
  return(a> b)?b:a;
}

laft::Size
laft::_max(const laft::Size&a,const laft::Size&b){
  return(a> b)?a:b;
}




void
laft::_generate_uniform_random_numbers(DBL*_el,const laft::Size&_s){
#if __cplusplus >= 201103L
  std::random_device rdu;
  std::mt19937 genu(rdu());
  std::uniform_real_distribution<> ud(0,1);
  for(Index i= 0;i!=_s;i++){
    _el[i]= ud(genu);
  }
#elif defined(__APPLE__) || defined(__linux__)
  srandom((unsigned)time(NULL));
  for(Index i= 0;i!=_s;i++){
    _el[i]= DBL(random())/DBL(RAND_MAX);
  }
#elif defined(_WIN32) || defined(_WIN64)
  srand((unsigned int)time(NULL));
  for(Index i= 0;i!=_s;i++){
    _el[i]= DBL(rand())/DBL(RAND_MAX);
  }
#endif
}




void
laft::_generate_normal_random_numbers(DBL*_el,const laft::Size&_s){
#if __cplusplus >= 201103L
  std::random_device rdn;
  std::mt19937 genn(rdn());
  std::normal_distribution<> nd(0,1);
  for(Index i= 0;i!=_s;i++){
    _el[i]= nd(genn);
  }
#else
  throw error::CplusplusElevenRequired();
#endif
}




Index
laft::indexOfMaximumAbsoluteElement(laft::Size sz,double*values){
  Index indexOfMax= 0UL;
  for(Index i= 1UL;i!=sz;i++){
    if(std::fabs(values[i])> std::fabs(values[indexOfMax])){
      indexOfMax= i;
    }
  }
  return indexOfMax;
}

Index
laft::indexOfMinimumAbsoluteElement(laft::Size sz,double*values){
  Index indexOfMin= 0UL;
  for(Index i= 1UL;i!=sz;i++){
    if(std::fabs(values[i])<std::fabs(values[indexOfMin])){
      indexOfMin= i;
    }
  }
  return indexOfMin;
}

Index
laft::indexOfMaximumElement(laft::Size sz,double*values){
  Index indexOfMax= 0UL;
  for(Index i= 1UL;i!=sz;i++){
    if(values[i]> values[indexOfMax]){
      indexOfMax= i;
    }
  }
  return indexOfMax;
}

Index
laft::indexOfMinimumElement(laft::Size sz,double*values){
  Index indexOfMin= 0UL;
  for(Index i= 1UL;i!=sz;i++){
    if(values[i]> values[indexOfMin]){
      indexOfMin= i;
    }
  }
  return indexOfMin;
}




Matrix
laft::eye(const laft::Size n){
  return Matrix::identity(n,n);
}

Matrix
laft::eye(const laft::Size m,const laft::Size n){
  return Matrix::identity(m,n);
}

Matrix
laft::zeros(const laft::Size n){
  return Matrix(n,n,ZEROS);
}

Matrix
laft::zeros(const laft::Size m,const laft::Size n){
  return Matrix(m,n,ZEROS);
}

Matrix
laft::ones(const laft::Size n){
  return Matrix(n,n,ONES);
}

Matrix
laft::ones(const laft::Size m,const laft::Size n){
  return Matrix(m,n,ONES);
}




Matrix
laft::diag(const Vector&v,const int k){
  return Matrix::diag(v,k);
}










void Array::setToleranceLevel(ToleranceLevel tl){
  TOLERANCE= EPS*std::pow(10,double(tl));
}

ToleranceLevel Array::toleranceLevel()const{
  int tl= int(std::log10(TOLERANCE/EPS));
  return ToleranceLevel(tl);
}

DBL Array::tolerance()const{
  return TOLERANCE;
}




void Array::increaseTolerance(){
  int tl= int(std::log10(TOLERANCE/EPS));
  if(tl<int(TL_TEN)){
    tl++;
  }
  TOLERANCE= EPS*std::pow(10.,double(tl));
}

void Array::decreaseTolerance(){
  int tl= int(std::log10(TOLERANCE/EPS));
  if(tl> int(TL_ZERO)){
    tl--;
  }
  TOLERANCE= EPS*std::pow(10.,double(tl));
}

void Array::initTolerance(){
  TOLERANCE= EPS;
}







laft::Size Array::size()const{
  return _size;
}

laft::Size Array::count()const{
  return _size;
}

DBL*Array::elements()const{
  return _elements;
}

void Array::setSize(laft::Size s){
  _size= s;
}

void Array::setElements(DBL*v){
  if(_elements!=v){
    delete[]_elements;
    _elements= v;
  }
}




void Array::copyValuesTo(double*val)const{
  _copy_elements(_size,_elements,val);
}







Array::Array()
:TOLERANCE(EPS),_size(0),_elements(NULL){}

Array::Array(const laft::Size theSize)
:TOLERANCE(EPS),_size(theSize){
  _elements= new DBL[_size];
  for(Index i= 0;i!=_size;i++){
    _elements[i]= 0.0;
  }
}





Array::Array(laft::Size theSize,InitialValues iv)
:TOLERANCE(EPS),_size(theSize){
  _elements= new DBL[_size];
  
  switch(iv){
    case ZEROS:
      for(Index i= 0;i!=_size;i++){
        _elements[i]= 0.0;
      }
      break;
      
    case ONES:
      for(Index i= 0;i!=_size;i++){
        _elements[i]= 1.0;
      }
      break;
      
    case RAND:
      _generate_uniform_random_numbers(_elements,_size);
      break;
      
    case RANDN:
      _generate_normal_random_numbers(_elements,_size);
      break;
  }
}




Array::Array(laft::Size theSize,double*values)
:TOLERANCE(EPS),_size(theSize){
  _elements= new DBL[_size];
  for(Index i= 0;i!=_size;i++){
    _elements[i]= DBL(values[i]);
  }
  _copy_elements(_size,values,_elements);
}




Array::Array(const Array&src)
:TOLERANCE(src.TOLERANCE),_size(src._size){
  _elements= new DBL[_size];
  _copy_elements(_size,src._elements,_elements);
}




Array::~Array(){
  if(_elements!=NULL){
    delete[]_elements;
  }
}







bool Array::operator==(const Array&rhs)const{
  if(this==&rhs){
    return true;
  }
  if(_size!=rhs._size){
    return false;
  }
  
  
  
  for(Index i= 0;i!=_size;i++){
    if(_elements[i]!=rhs._elements[i]){
      return false;
    }
  }
  return true;
  
  
  
  ;
}




bool Array::isSimilarTo(const Array&a)const{
  if(*this==a){
    return true;
  }
  
  if(_size!=a._size){
    return false;
  }
  
  
  
  
  for(Index i= 0;i!=_size;i++){
    if(std::fabs(_elements[i]-a._elements[i])> TOLERANCE){
      return false;
    }
  }
  return true;
  
  
  
  
  ;
}




bool Array::operator!=(const Array&rhs)const{
  if(*this==rhs){
    return false;
  }else{
    return true;
  }
}



;



void Array::print(PrintFormat fmt,std::ostream&strm)const{
  
  
  
  strm<<std::endl;
  strm<<" ARRAY @ "<<(void*)this<<std::endl;
  strm<<" -Size: "<<_size<<std::endl;
  char prev= strm.fill('-');
  strm<<std::setw(80)<<'-'<<std::endl;
  strm.fill(prev);
  
  
  
  
  
  ;
  int width= _set_print_format(fmt,strm);
  
  
  
  for(Index i= 0;i!=_size;i++){
    strm.width(width);
    strm<<_elements[i]<<std::endl;
  }
  
  
  
  
  ;
  _restore_print_format(strm);
}




int
Array::_set_print_format(PrintFormat fmt,std::ostream&strm)const{
  int precision;
  int width;
  std::ios_base::fmtflags options;
  
  switch(fmt){
    case SHORT:
      options= std::ios_base::fixed;
      precision= 2;
      width= 8;
      break;
      
    case NORMAL:
      options= std::ios_base::fixed;
      precision= 4;
      width= 10;
      break;
      
    case EXTENDED:
      options= std::ios_base::fixed;
      precision= 8;
      width= 14;
      break;
      
    case SCIENTIFIC:
      options= std::ios_base::scientific;
      precision= 4;
      width= 10;
      break;
      
    case EXTSCIENTIFIC:
      options= std::ios_base::scientific;
      precision= 8;
      width= 18;
  }
  
  strm.setf(options,std::ios_base::floatfield);
  strm.precision(precision);
  return width;
}




void
Array::_restore_print_format(std::ostream&strm)const{
  strm.setf(std::ios_base::fmtflags(0),std::ios_base::floatfield);
  strm.precision(6);
  strm.width(0);
}




std::ostream&laft::operator<<(std::ostream&strm,const Array&theArray){
  theArray.print(SHORT,strm);
  return strm;
}



;












VectorType Vector::type()const{
  return _type;
}

void Vector::setType(VectorType t){
  _type= t;
}

bool Vector::isRowVector()const{
  if(_type==ROW_VECTOR){
    return true;
  }else{
    return false;
  }
}

bool Vector::isColumnVector()const{
  if(_type==COLUMN_VECTOR){
    return true;
  }else{
    return false;
  }
}

laft::Size Vector::dim()const{
  return _size;
}

laft::Size Vector::dimension()const{
  return _size;
}







Vector::Vector():_type(COLUMN_VECTOR){}




Vector::Vector(laft::Size theSize,VectorType type)
:_type(type),Array(theSize){
}




Vector::Vector(laft::Size theSize,InitialValues iv,VectorType type)
:_type(type),Array(theSize,iv){
}




Vector::Vector(laft::Size theSize,double*values,VectorType type)
:_type(type),Array(theSize,values){
}




Vector::Vector(const Vector&src)
:_type(src.type()),Array(src){
}




Vector::Vector(const char*fmt){
  _format(fmt);
}




Vector::~Vector(){
}







const DBL&
Vector::operator()(const Index&i)const{
  if(doesIndexExceedVectorDimension(i)){
    throw error::OutOfRangeIndex();
  }
  return _elements[i-1];
}

DBL&
Vector::operator()(const Index&i){
  return const_cast<DBL&> (static_cast<const Vector&> (*this)(i));
}




bool
Vector::doesIndexExceedVectorDimension(const Index&i)const{
  if((i<1)||(_size<i)){
    return true;
  }else{
    return false;
  }
}




Vector&Vector::operator= (const Vector&src){
  if(this==&src){
    return*this;
  }
  delete[]_elements;
  _size= src._size;
  _type= src._type;
  _elements= new DBL[_size];
  _copy_elements(_size,src._elements,_elements);
  
  return*this;
}




bool
Vector::operator==(const Vector&rhs)const{
  if(this==&rhs){
    return true;
  }
  if(_type!=rhs.type()){
    return false;
  }
  return Array::operator==(rhs);
}




bool
Vector::isSimilarTo(const Vector&v)const{
  if(*this==v){
    return true;
  }
  
  if(_type!=v.type()){
    return false;
  }
  
  return Array::isSimilarTo(v);
}




bool
Vector::operator!=(const Vector&rhs)const{
  if(*this==rhs){
    return false;
  }else{
    return true;
  }
}




Vector Vector::operator+(const Vector&rhs)const{
  
  
  
  if(_size!=rhs._size){
    throw error::DimensionMismatch();
  }
  
  if(_type!=rhs._type){
    throw error::NotPermitted();
  }
  
  
  
  
  
  ;
  Vector result(_size,_type);
  _add_elements(_size,_elements,rhs._elements,result._elements);
  return result;
}

Vector Vector::operator-(const Vector&rhs)const{
  
  
  
  if(_size!=rhs._size){
    throw error::DimensionMismatch();
  }
  
  if(_type!=rhs._type){
    throw error::NotPermitted();
  }
  
  
  
  
  
  ;
  Vector result(_size,_type);
  _subtract_elements(_size,_elements,rhs._elements,result._elements);
  return result;
}

Vector&Vector::operator+= (const Vector&rhs){
  
  
  
  if(_size!=rhs._size){
    throw error::DimensionMismatch();
  }
  
  if(_type!=rhs._type){
    throw error::NotPermitted();
  }
  
  
  
  
  
  ;
  _add_elements(_size,_elements,rhs._elements,_elements);
  return*this;
}

Vector&Vector::operator-= (const Vector&rhs){
  
  
  
  if(_size!=rhs._size){
    throw error::DimensionMismatch();
  }
  
  if(_type!=rhs._type){
    throw error::NotPermitted();
  }
  
  
  
  
  
  ;
  _subtract_elements(_size,_elements,rhs._elements,_elements);
  return*this;
}







Vector Vector::operator+(const double s)const{
  Vector result(_size,_type);
  for(Index i= 0;i!=_size;i++){
    result._elements[i]= _elements[i]+s;
  }
  return result;
}

Vector Vector::operator-(const double s)const{
  Vector result(_size,_type);
  for(Index i= 0;i!=_size;i++){
    result._elements[i]= _elements[i]-s;
  }
  return result;
}

Vector&Vector::operator+= (const double s){
  for(Index i= 0;i!=_size;i++){
    _elements[i]+= s;
  }
  return*this;
}

Vector&Vector::operator-= (const double s){
  for(Index i= 0;i!=_size;i++){
    _elements[i]-= s;
  }
  return*this;
}




Vector Vector::operator-()const{
  Vector result(_size,_type);
  _negate_elements(_size,_elements,result._elements);
  return result;
}




Vector Vector::operator*(const double&s)const{
  Vector result(_size,_type);
  _multiply_scalar_to_elements(_size,_elements,DBL(s),result._elements);
  return result;
}

Vector Vector::operator/(const double&s)const{
  if(fabs(s)<TOLERANCE){
    throw error::DivisionByZero();
  }
  Vector result(_size,_type);
  _divide_elements_by_scalar(_size,_elements,DBL(s),result._elements);
  return result;
}

Vector&Vector::operator*= (const double&s){
  _multiply_scalar_to_elements(_size,_elements,DBL(s),_elements);
  return*this;
}

Vector&Vector::operator/= (const double&s){
  if(fabs(s)<TOLERANCE){
    throw error::DivisionByZero();
  }
  _divide_elements_by_scalar(_size,_elements,DBL(s),_elements);
  return*this;
}




Vector
Vector::operator*(const Matrix&m)const{
  if(_type==COLUMN_VECTOR){
    throw error::NotPermitted();
  }
  
  if(_size!=m.height()){
    throw error::DimensionMismatch();
  }
  
  laft::Size h= m.height();
  laft::Size w= m.width();
  DBL*_m= m.elements();
  DBL*r= new DBL[w];
  double a= 0.;
  
  for(Index j= 0;j!=w;j++){
    for(Index i= 0;i!=h;i++){
      a+= _elements[i]*_m[j*h+i];
    }
    r[j]= a;
    a= 0.;
  }
  
  Vector R(w,r,ROW_VECTOR);
  delete[]r;
  return R;
}







void
Vector::insertElementAt(const double&v,const Index idx){
  if((_size==0)&&(idx==1)){
    appendElement(v);
    return;
  }else if(doesIndexExceedVectorDimension(idx)){
    throw error::OutOfRangeIndex();
  }
  DBL*nel= new DBL[_size+1];
  
  _copy_elements(idx-1,_elements,nel);
  
  nel[idx-1]= v;
  _copy_elements(_size-idx+1,&_elements[idx-1],&nel[idx]);
  
  
  if(_elements!=NULL){
    delete[]_elements;
  }
  _elements= nel;
  _size++;
}




void
Vector::appendElement(const double&v){
  DBL*nel= new DBL[_size+1];
  _copy_elements(_size,_elements,nel);
  nel[_size]= v;
  if(_elements!=NULL){
    delete[]_elements;
  }
  _elements= nel;
  _size++;
}




void
Vector::insertVectorAt(const Vector&v,const Index idx){
  
  
  
  if(_type!=v.type()){
    throw error::DimensionMismatch();
  }
  
  
  
  
  
  if((_size==0)&&(idx==1)){
    appendVector(v);
    return;
  }else if(doesIndexExceedVectorDimension(idx)){
    throw error::OutOfRangeIndex();
  }
  
  DBL*nel= new DBL[_size+v._size];
  _copy_elements(idx-1,_elements,nel);
  _copy_elements(v._size,v._elements,&nel[idx-1]);
  _copy_elements(_size-idx+1,&_elements[idx-1],&nel[idx-1+v._size]);
  
  
  delete[]_elements;
  _elements= nel;
  _size+= v._size;
}




void
Vector::appendVector(const Vector&v){
  DBL*nel= new DBL[_size+v._size];
  _copy_elements(_size,_elements,nel);
  _copy_elements(v._size,v._elements,&nel[_size]);
  
  if(_elements!=NULL){
    delete[]_elements;
  }
  _elements= nel;
  _size+= v._size;
}




void
Vector::removeElementAt(const Index idx){
  if(doesIndexExceedVectorDimension(idx)){
    throw error::OutOfRangeIndex();
  }
  
  DBL*nel;
  if(_size==1){
    nel= NULL;
  }else{
    nel= new DBL[_size-1];
    _copy_elements(idx-1,_elements,nel);
    _copy_elements(_size-idx,&_elements[idx],&nel[idx-1]);
    
  }
  delete[]_elements;
  _elements= nel;
  _size--;
}




void
Vector::removeElementsInRange(const Index begin,const Index end){
  if(doesIndexExceedVectorDimension(begin)||doesIndexExceedVectorDimension(end)){
    throw error::OutOfRangeIndex();
  }
  if(begin> end){
    throw error::NotPermitted();
  }
  DBL*nel= new DBL[_size-end+begin-1];
  _copy_elements(begin-1,_elements,nel);
  
  _copy_elements(_size-end,&_elements[end],&nel[begin-1]);
  delete[]_elements;
  _elements= nel;
  _size-= (end-begin+1);
}







double Vector::norm(const unsigned p)const{
  if(p==0){
    throw error::NotPermitted();
  }
  double normOfVector;
  if(p==Inf){
    
    
    
    Index maxIndexInVector= laft::indexOfMaximumAbsoluteElement(_size,_elements);
    normOfVector= _elements[maxIndexInVector];
    
    
    
    
    ;
  }else{
    
    
    
    double reciprocalP= 1.0/double(p);
    double poweredSumOfElements= 0.0;
    for(Index i= 0;i!=_size;i++){
      poweredSumOfElements+= std::pow(_elements[i],double(p));
    }
    normOfVector= std::pow(poweredSumOfElements,reciprocalP);
    
    
    
    
    ;
  }
  return normOfVector;
}




Vector
Vector::unit(const unsigned p)const{
  Vector u= (*this);
  double n= this->norm(p);
  if(n> TOLERANCE){
    u/= n;
  }
  return u;
}




Vector
Vector::normalize(const unsigned p){
  double n= this->norm(p);
  if(n> TOLERANCE){
    (*this)/= n;
  }
  return(*this);
}




Vector Vector::transpose()const{
  Vector transposed(*this);
  if(_type==ROW_VECTOR){
    transposed.setType(COLUMN_VECTOR);
  }else{
    transposed.setType(ROW_VECTOR);
  }
  return transposed;
}

Vector Vector::T()const{
  return this->transpose();
}




double Vector::operator*(const Vector&v)const{
  if(_size!=v._size){
    throw error::DimensionMismatch();
  }
  if(isRowVector()&&v.isColumnVector()){
    double ip= 0.0;
    for(Index i= 0;i!=_size;i++){
      ip+= _elements[i]*v._elements[i];
    }
    return ip;
  }else{
    throw error::NotPermitted();
  }
}







void Vector::print(PrintFormat fmt,std::ostream&strm)const{
  
  
  
  strm<<std::endl;
  if(_size==0){
    strm<<" Empty vector"<<std::endl;
  }else{
    strm<<" "<<_size<<" dimensional "<<
    ((_type==ROW_VECTOR)?"row vector":"column vector")<<std::endl;
  }
  char prev= strm.fill('-');
  strm<<std::setw(80)<<'-'<<std::endl;
  strm.fill(prev);
  
  
  
  
  
  ;
  int width= _set_print_format(fmt,strm);
  
  
  
  for(Index i= 0;i!=_size;i++){
    strm.width(width);
    if(_type==ROW_VECTOR){
      strm<<_elements[i];
    }else{
      strm<<_elements[i]<<std::endl;
    }
  }
  if(_type==ROW_VECTOR){
    strm<<std::endl;
  }
  strm<<std::endl<<std::endl;
  
  
  
  
  
  
  
  
  
  
  
  
  ;
  _restore_print_format(strm);
}







Vector
laft::operator+(const double s,const Vector&v){
  return v+s;
}

Vector
laft::operator-(const double s,const Vector&v){
  return-v+s;
}




Vector
laft::operator*(const double&s,const Vector&A){
  laft::Size sz= A.size();
  Vector result(sz,A.type());
  laft::_multiply_scalar_to_elements(sz,A.elements(),DBL(s),result.elements());
  return result;
}




double
laft::norm(const Vector&v,const unsigned p){
  return v.norm(p);
}

Vector
laft::unit(const Vector&v,const unsigned p){
  return v.unit();
}

Vector
laft::normalize(Vector&v,const unsigned p){
  return v.normalize(p);
}




Vector
laft::transpose(const Vector&v){
  return v.transpose();
}




double
laft::dot(const Vector&v1,const Vector&v2){
  if(v1.dim()!=v2.dim()){
    throw laft::error::DimensionMismatch();
  }
  
  DBL*_v1= v1.elements();
  DBL*_v2= v2.elements();
  double ip= 0.0;
  
  for(Index i= 0;i!=v1.dim();i++){
    ip+= _v1[i]*_v2[i];
  }
  
  return ip;
}







void
Vector::_format(const char*fmt){
  
  
  
  bool isColumnVector= false;
  bool isRowVector= false;
  bool delimiterPreceded= false;
  bool onlyWhiteSpacePreceded= false;
  bool numericRepPreceded= false;
  bool endOfLine= false;
  char ch;
  laft::Size count= 0;
  unsigned idx= 0;
  
  while(!endOfLine){
    ch= fmt[idx++];
    if(ch==','||ch==';'){
      delimiterPreceded= true;
      onlyWhiteSpacePreceded= false;
    }
    if((ch=='\t'||ch==' ')&&delimiterPreceded==false)onlyWhiteSpacePreceded= true;
    if(ch=='\t'||ch==' '||ch==','||ch==';'||ch=='\0'){
      
      
      
      if(ch==';'&&isRowVector==true){
        throw error::IllegalConstruction();
      }
      if(ch==','&&isColumnVector==true){
        throw error::IllegalConstruction();
      }
      
      
      
      
      ;
      if(numericRepPreceded==true){
        
        
        
        
        if(ch==';')isColumnVector= true;
        if(ch==',')isRowVector= true;
        count++;
        numericRepPreceded= false;
        
        
        
        
        
        ;
      }
      if(ch=='\0'){
        endOfLine= true;
      }
    }else{
      
      
      
      if(count==0&&delimiterPreceded==true)throw error::IllegalConstruction();
      
      
      
      
      
      ;
      
      
      
      if(onlyWhiteSpacePreceded==true){
        if(isColumnVector==true){
          throw error::IllegalConstruction();
        }else{
          isRowVector= true;
        }
      }
      numericRepPreceded= true;
      delimiterPreceded= false;
      onlyWhiteSpacePreceded= false;
      
      
      
      
      
      ;
    }
  }
  _size= count;
  if(isRowVector==true){
    _type= ROW_VECTOR;
  }else{
    _type= COLUMN_VECTOR;
  }
  
  
  
  ;
  
  
  
  if(_size> 0){
    _elements= new DBL[_size];
    for(Index i= 0;i!=_size;i++)_elements[i]= 0.0;
  }else _elements= NULL;
  
  char buf[128];
  endOfLine= false;
  idx= 0;
  unsigned ddx= 0;
  unsigned i= 0;
  while(!endOfLine){
    ch= fmt[idx++];
    if(ch=='\t'||ch==' '||ch==','||ch==';'||ch=='\0'){
      if(ddx> 0){
        buf[ddx]= '\0';
        _elements[i]= atof(buf);
        i++;
        ddx= 0;
      }
      if(ch=='\0')endOfLine= true;
    }else buf[ddx++]= ch;
  }
  
  
  
  
  ;
}
















laft::Size Matrix::width()const{
  return _width;
}

laft::Size Matrix::numberOfColumns()const{
  return _width;
}

laft::Size Matrix::numCols()const{
  return _width;
}

laft::Size Matrix::height()const{
  return _height;
}

laft::Size Matrix::numberOfRows()const{
  return _height;
}

laft::Size Matrix::numRows()const{
  return _height;
}

void Matrix::setDimensions(laft::Size h,laft::Size w){
  _width= w;
  _height= h;
  this->recount();
}







Matrix::Matrix()
:Array(),_width(0),_height(0){}

Matrix::Matrix(const laft::Size h,const laft::Size w)
:Array(h*w),_width(w),_height(h){
}




Matrix::Matrix(const laft::Size h,const laft::Size w,const InitialValues iv)
:Array(h*w,iv),_width(w),_height(h){
}




Matrix::Matrix(const laft::Size h,const laft::Size w,double*values)
:Array(h*w,values),_width(w),_height(h){
}




Matrix::Matrix(const Matrix&src)
:Array(src),_width(src._width),_height(src._height){
}




Matrix::Matrix(const char*fmt){
  _format(fmt);
}







const DBL&
Matrix::operator()(const Index&i,const Index&j)const{
  if(doesIndicesExceedMatrixDimension(i,j)){
    throw error::OutOfRangeIndex();
  }
  return _elements[(j-1)*_height+(i-1)];
}

DBL&
Matrix::operator()(const Index&i,const Index&j){
  return const_cast<DBL&> (static_cast<const Matrix&> (*this)(i,j));
}





Matrix&
Matrix::operator= (const Matrix&src){
  if(this==&src){
    return*this;
  }
  
  delete[]_elements;
  _elements= new DBL[src.size()];
  _height= src.height();
  _width= src.width();
  recount();
  _copy_elements(_size,src._elements,_elements);
  
  return*this;
}




bool
Matrix::operator==(const Matrix&m)const{
  if(this==&m){
    return true;
  }
  if(this->hasSameDimensionsWith(m)==false){
    return false;
  }
  return Array::operator==(m);
}

bool
Matrix::operator!=(const Matrix&m)const{
  if((*this)==m){
    return false;
  }else{
    return true;
  }
}

bool
Matrix::isSimilarTo(const Matrix&m)const{
  if(*this==m){
    return true;
  }
  if(this->hasSameDimensionsWith(m)==false){
    return false;
  }
  return Array::isSimilarTo(m);
}




Matrix
Matrix::operator+(const Matrix&m)const{
  if(this->hasSameDimensionsWith(m)==false){
    throw error::DimensionMismatch();
  }
  
  Matrix r(_height,_width);
  _add_elements(_size,_elements,m._elements,r._elements);
  return r;
}

Matrix
Matrix::operator-(const Matrix&m)const{
  if(this->hasSameDimensionsWith(m)==false){
    throw error::DimensionMismatch();
  }
  
  Matrix r(_height,_width);
  _subtract_elements(_size,_elements,m._elements,r._elements);
  return r;
}

Matrix&
Matrix::operator+= (const Matrix&m){
  if(this->hasSameDimensionsWith(m)==false){
    throw error::DimensionMismatch();
  }
  
  _add_elements(_size,_elements,m._elements,_elements);
  return*this;
}

Matrix&
Matrix::operator-= (const Matrix&m){
  if(this->hasSameDimensionsWith(m)==false){
    throw error::DimensionMismatch();
  }
  
  _subtract_elements(_size,_elements,m._elements,_elements);
  return*this;
}






Matrix
Matrix::operator+(const double s)const{
  Matrix r(_height,_width);
  for(Index p= 0;p!=_size;p++){
    r._elements[p]= _elements[p]+s;
  }
  return r;
}

Matrix
Matrix::operator-(const double s)const{
  Matrix r(_height,_width);
  for(Index p= 0;p!=_size;p++){
    r._elements[p]= _elements[p]-s;
  }
  return r;
}

Matrix&
Matrix::operator+= (const double s){
  for(Index p= 0;p!=_size;p++){
    _elements[p]+= s;
  }
  return*this;
}

Matrix&
Matrix::operator-= (const double s){
  for(Index p= 0;p!=_size;p++){
    _elements[p]-= s;
  }
  return*this;
}





Matrix
Matrix::operator-()const{
  Matrix r(_height,_width);
  _negate_elements(_size,_elements,r._elements);
  return r;
}




Matrix
Matrix::operator*(const double&s)const{
  Matrix r(_height,_width);
  _multiply_scalar_to_elements(_size,_elements,s,r._elements);
  return r;
}

Matrix
Matrix::operator*(const Matrix&m)const{
  if(_width!=m._height){
    throw error::DimensionMismatch();
  }
  
  DBL*r= new DBL[_height*m._width];
  
  double a= 0.;
  
  for(Index i= 0;i!=_height;i++){
    for(Index j= 0;j!=m._width;j++){
      for(Index k= 0;k!=_width;k++){
        a+= _elements[k*_height+i]*m._elements[j*m._height+k];
      }
      r[j*_height+i]= a;
      a= 0.0;
    }
  }
  
  Matrix R(_height,m._width,r);
  delete[]r;
  return R;
}

Vector
Matrix::operator*(const Vector&v)const{
  if(v.isRowVector()==true){
    throw error::NotPermitted();
  }
  
  if(_width!=v.dim()){
    throw error::DimensionMismatch();
  }
  
  DBL*r= new DBL[_height];
  DBL*_v= v.elements();
  double a= 0.;
  
  for(Index i= 0;i!=_height;i++){
    for(Index j= 0;j!=_width;j++){
      a+= _elements[j*_height+i]*_v[j];
    }
    r[i]= a;
    a= 0.;
  }
  
  Vector R(_height,r);
  delete[]r;
  return R;
}


Matrix&
Matrix::operator*= (const double&s){
  _multiply_scalar_to_elements(_size,_elements,s,_elements);
  return*this;
}

Matrix&
Matrix::operator*= (const Matrix&m){
  if(_width!=m._height){
    throw error::DimensionMismatch();
  }
  
  DBL*r= new DBL[_height*m._width];
  
  
  double a= 0.0;
  
  for(Index i= 0;i!=_height;i++){
    for(Index j= 0;j!=m._width;j++){
      for(Index k= 0;k!=_width;k++){
        a+= _elements[k*_height+i]*m._elements[j*m._height+k];
      }
      r[j*_height+i]= a;
      a= 0.0;
    }
  }
  this->setDimensions(_height,m._width);
  _copy_elements(_size,r,_elements);
  delete[]r;
  return*this;
}




Matrix
Matrix::operator/(const double&s)const{
  if(s==0.){
    throw error::DivisionByZero();
  }
  return(*this)*(1.0/s);
}








void
Matrix::insertRowVectorAt(const Vector&v,const Index p){
  if((_size==0)&&(v.isRowVector()==true)){
    appendRowVector(v);
    return;
  }
  
  
  
  
  if(v.isRowVector()==false||v.dim()!=_width){
    throw error::DimensionMismatch();
  }
  
  if(doesIndexExceedMatrixRowDimension(p)==true){
    throw error::OutOfRangeIndex();
  }
  
  if(_width==0){
    throw error::NotPermitted();
  }
  
  
  
  ;
  
  DBL*nel= new DBL[(_height+1)*_width];
  
  _replace_rows_with_matrix(nel,_height+1,_width,1,p-1,*this,1);
  
  
  
  
  
  DBL*_v= v.elements();
  for(Index i= 0;i!=v.dim();i++){
    nel[i*(_height+1)+p-1]= _v[i];
  }
  
  
  
  
  
  
  
  ;
  
  _replace_rows_with_matrix(nel,_height+1,_width,p+1,_height-p+1,*this,p);
  
  
  _height++;
  recount();
  delete[]_elements;
  _elements= nel;
}




void
Matrix::insertRowsAt(const Matrix&m,const Index p){
  if(_size==0){
    appendRows(m);
    return;
  }
  
  
  
  
  if(m._width!=_width){
    throw error::DimensionMismatch();
  }
  
  if(doesIndexExceedMatrixRowDimension(p)==true){
    throw error::OutOfRangeIndex();
  }
  
  if(_width==0){
    throw error::NotPermitted();
  }
  
  
  
  
  
  ;
  
  laft::Size inc= m._height;
  DBL*nel= new DBL[(_height+m._height)*_width];
  
  _replace_rows_with_matrix(nel,_height+inc,_width,1,p-1,*this,1);
  
  
  _replace_rows_with_matrix(nel,_height+inc,_width,p,inc,m,1);
  
  
  _replace_rows_with_matrix(nel,_height+inc,_width,p+inc,_height-p+1,*this,p);
  
  
  _height+= inc;
  recount();
  delete[]_elements;
  _elements= nel;
}




void
Matrix::appendRowVector(const Vector&v){
  if((_size==0)&&(v.isRowVector()==true)){
    _size= _width= v.dim();
    _height= 1;
    _elements= new DBL[_size];
    _copy_elements(_size,v.elements(),_elements);
    return;
  }
  
  
  
  
  if(v.isRowVector()==false||v.dim()!=_width){
    throw error::DimensionMismatch();
  }
  
  if(_width==0){
    throw error::NotPermitted();
  }
  
  
  
  
  
  ;
  
  DBL*nel= new DBL[(_height+1)*_width];
  
  _replace_rows_with_matrix(nel,_height+1,_width,1,_height,*this,1);
  
  
  
  
  
  DBL*_v= v.elements();
  for(Index i= 0;i!=v.dim();i++){
    nel[i*(_height+1)+_height]= _v[i];
  }
  
  
  
  
  
  
  ;
  
  _height++;
  recount();
  delete[]_elements;
  _elements= nel;
}




void
Matrix::appendRows(const Matrix&m){
  if(_size==0){
    _size= m.size();
    _width= m.width();
    _height= m.height();
    _elements= new DBL[_size];
    _copy_elements(_size,m.elements(),_elements);
    return;
  }
  
  
  
  
  if(_width!=m.width()){
    throw error::DimensionMismatch();
  }
  
  if(_width==0){
    throw error::NotPermitted();
  }
  
  
  
  
  
  
  
  ;
  
  laft::Size inc= m.height();
  DBL*nel= new DBL[(_height+inc)*_width];
  
  _replace_rows_with_matrix(nel,_height+inc,_width,1,_height,*this,1);
  
  
  _replace_rows_with_matrix(nel,_height+inc,_width,_height+1,inc,m,1);
  
  
  _height+= inc;
  recount();
  delete[]_elements;
  _elements= nel;
}




void
Matrix::deleteRowAt(const Index p){
  if(doesIndexExceedMatrixRowDimension(p)==true){
    throw error::OutOfRangeIndex();
  }
  
  if(_height==1){
    _size= _height= _width= 0;
    delete[]_elements;
    _elements= NULL;
    return;
  }
  
  DBL*nel= new DBL[(_height-1)*_width];
  _replace_rows_with_matrix(nel,_height-1,_width,1,p-1,*this,1);
  
  _replace_rows_with_matrix(nel,_height-1,_width,p,_height-p,*this,p+1);
  
  _height--;
  recount();
  delete[]_elements;
  _elements= nel;
}







void
Matrix::swapRows(const Index p,const Index q){
  if((p==q)||(_width==0)){
    return;
  }
  if((doesIndexExceedMatrixRowDimension(p)==true)||
     (doesIndexExceedMatrixRowDimension(q)==true)){
    throw error::OutOfRangeIndex();
  }
  for(Index j= 0;j!=_width;j++){
    DBL temp= _elements[_height*j+p-1];
    _elements[_height*j+p-1]= _elements[_height*j+q-1];
    _elements[_height*j+q-1]= temp;
  }
}





void
Matrix::insertColumnVectorAt(const Vector&v,const Index p){
  if((_size==0)&&(v.isColumnVector()==true)){
    appendColumnVector(v);
    return;
  }
  
  
  
  
  if(v.isRowVector()==true||v.dim()!=_height){
    throw error::DimensionMismatch();
  }
  
  if(doesIndexExceedMatrixColumnDimension(p)==true){
    throw error::OutOfRangeIndex();
  }
  
  if(_height==0){
    throw error::NotPermitted();
  }
  
  
  
  
  ;
  DBL*nel= new DBL[_height*(_width+1)];
  
  _replace_columns_with_matrix(nel,_height,_width+1,1,p-1,*this,1);
  
  
  
  
  
  DBL*_v= v.elements();
  for(Index j= 0;j!=v.dim();j++){
    nel[(p-1)*_height+j]= _v[j];
  }
  
  
  
  
  
  ;
  
  _replace_columns_with_matrix(nel,_height,_width+1,p+1,_width-p+1,*this,p);
  
  
  _width++;
  recount();
  delete[]_elements;
  _elements= nel;
}






void
Matrix::insertColumnsAt(const Matrix&m,const Index p){
  if(_size==0){
    appendColumns(m);
    return;
  }
  
  
  
  
  if(m._height!=_height){
    throw error::DimensionMismatch();
  }
  
  if(doesIndexExceedMatrixColumnDimension(p)==true){
    throw error::OutOfRangeIndex();
  }
  
  if(_height==0){
    throw error::NotPermitted();
  }
  
  
  
  
  
  
  ;
  laft::Size inc= m._width;
  DBL*nel= new DBL[_height*(_width+m._width)];
  
  _replace_columns_with_matrix(nel,_height,_width+inc,1,p-1,*this,1);
  
  
  _replace_columns_with_matrix(nel,_height,_width+inc,p,inc,m,1);
  
  
  _replace_columns_with_matrix(nel,_height,_width+inc,p+inc,_width-p+1,*this,p);
  
  
  _width+= inc;
  recount();
  delete[]_elements;
  _elements= nel;
}






void
Matrix::appendColumnVector(const Vector&v){
  if((_size==0)&&(v.isColumnVector()==true)){
    _height= _size= v.dim();
    _width= 1;
    _elements= new DBL[_size];
    _copy_elements(_size,v.elements(),_elements);
    return;
  }
  
  
  
  
  if(v.isRowVector()==true||v.dim()!=_height){
    throw error::DimensionMismatch();
  }
  if(_height==0){
    throw error::NotPermitted();
  }
  
  
  
  ;
  
  DBL*nel= new DBL[_height*(_width+1)];
  _replace_columns_with_matrix(nel,_height,_width+1,1,_width,*this,1);
  
  
  
  
  
  DBL*_v= v.elements();
  for(Index j= 0;j!=v.dim();j++){
    nel[_height*_width+j]= _v[j];
  }
  
  
  
  
  
  
  ;
  
  _width++;
  recount();
  delete[]_elements;
  _elements= nel;
}




void
Matrix::appendColumns(const Matrix&m){
  if(_size==0){
    _size= m.size();
    _width= m.width();
    _height= m.height();
    _elements= new DBL[_size];
    _copy_elements(_size,m.elements(),_elements);
    return;
  }
  
  
  
  
  if(_height!=m.height()){
    throw error::DimensionMismatch();
  }
  
  if(_height==0){
    throw error::NotPermitted();
  }
  
  
  
  
  
  
  
  ;
  laft::Size inc= m.width();
  DBL*nel= new DBL[_height*(_width+inc)];
  
  _replace_columns_with_matrix(nel,_height,_width+inc,1,_width,*this,1);
  
  
  _replace_columns_with_matrix(nel,_height,_width+inc,_width+1,inc,m,1);
  
  
  _width+= inc;
  recount();
  delete[]_elements;
  _elements= nel;
}




void
Matrix::deleteColumnAt(const Index p){
  if(doesIndexExceedMatrixColumnDimension(p)==true){
    throw error::OutOfRangeIndex();
  }
  
  if(_width==1){
    _size= _height= _width= 0;
    delete[]_elements;
    _elements= NULL;
    return;
  }
  
  DBL*nel= new DBL[_height*(_width-1)];
  
  _replace_columns_with_matrix(nel,_height,_width-1,1,p-1,*this,1);
  
  
  _replace_columns_with_matrix(nel,_height,_width-1,p,_width-p,*this,p+1);
  
  
  _width--;
  recount();
  delete[]_elements;
  _elements= nel;
}







void
Matrix::swapColumns(const Index p,const Index q){
  if((p==q)||(_height==0)){
    return;
  }
  if((doesIndexExceedMatrixColumnDimension(p)==true)||
     (doesIndexExceedMatrixColumnDimension(q)==true)){
    throw error::OutOfRangeIndex();
  }
  for(Index i= 0;i!=_height;i++){
    DBL temp= _elements[_height*(p-1)+i];
    _elements[_height*(p-1)+i]= _elements[_height*(q-1)+i];
    _elements[_height*(q-1)+i]= temp;
  }
}










Vector
Matrix::row(const Index p)const{
  if(doesIndexExceedMatrixRowDimension(p)==true){
    throw error::OutOfRangeIndex();
  }
  
  DBL*el= new DBL[_width];
  for(Index j= 0;j!=_width;j++){
    el[j]= _elements[_height*j+p-1];
  }
  
  Vector v(_width,el,ROW_VECTOR);
  delete[]el;
  
  return v;
}

Vector
Matrix::column(const Index p)const{
  if(doesIndexExceedMatrixColumnDimension(p)==true){
    throw error::OutOfRangeIndex();
  }
  
  DBL*el= new DBL[_height];
  for(Index i= 0;i!=_height;i++){
    el[i]= _elements[_height*(p-1)+i];
  }
  
  Vector v(_height,el,COLUMN_VECTOR);
  delete[]el;
  
  return v;
}




Vector
Matrix::diag()const{
  INT n= _min(_width,_height);
  DBL*el= new DBL[n];
  
  for(Index i= 0;i!=n;i++){
    el[i]= _elements[_height*i+i];
  }
  
  Vector v(n,el,COLUMN_VECTOR);
  delete[]el;
  
  return v;
}




Matrix
Matrix::subMatrix(const Index top,const Index bottom,
                  const Index left,const Index right)const{
  
  
  
  
  if((doesIndexExceedMatrixRowDimension(top)==true)||
     (doesIndexExceedMatrixRowDimension(bottom)==true)||
     (doesIndexExceedMatrixColumnDimension(left)==true)||
     (doesIndexExceedMatrixColumnDimension(right)==true)){
    throw error::OutOfRangeIndex();
  }
  
  if((right<left)||(bottom<top)){
    throw error::NotPermitted();
  }
  
  
  
  ;
  Matrix m(bottom-top+1,right-left+1);
  DBL*el= m.elements();
  
  for(Index j= left-1;j!=right;j++){
    for(Index i= top-1;i!=bottom;i++){
      *el= _elements[_height*j+i];
      el++;
    }
  }
  return m;
}





Matrix
Matrix::rows(const Index top,const Index bottom)const{
  return subMatrix(top,bottom,1,_width);
}

Matrix
Matrix::columns(const Index left,const Index right)const{
  return subMatrix(1,_height,left,right);
}







Matrix
Matrix::transpose()const{
  DBL*nel= new DBL[_size];
  
  for(Index i= 0;i!=_height;i++){
    for(Index j= 0;j!=_width;j++){
      nel[i*_width+j]= _elements[j*_height+i];
    }
  }
  Matrix m= Matrix(_width,_height,nel);
  return m;
}

Matrix
Matrix::T()const{
  return this->transpose();
}




Matrix
Matrix::inv()const{
  if(_height!=_width){
    throw error::NotPermitted();
  }
  
  INT N= _height;
  Matrix m(*this);
  INT INFO;
  INT*IPIV= new INT[_height];
  
  
  
  
  dgetrf_(&N,&N,m.elements(),&N,IPIV,&INFO);
  
  
  
  
  ;
  
  
  
  double d= m._prod_diagonal();
  if(std::fabs(d)<TOLERANCE){
    throw error::Singular();
  }else{
    INT N= _height;
    INT prod= _height*_height;
    DBL*WORK= new DBL[_height];
    
    dgetri_(&N,m.elements(),&N,IPIV,WORK,&prod,&INFO);
    
    delete[]WORK;
  }
  
  
  
  ;
  
  delete[]IPIV;
  return m;
}








bool Matrix::isSquare()const{
  if(_width==_height){
    return true;
  }else{
    return false;
  }
}

bool Matrix::isSymmetric()const{
  if(*this==this->transpose()){
    return true;
  }else{
    return false;
  }
}

bool Matrix::isSkewSymmetric()const{
  if(*this==-(this->transpose())){
    return true;
  }else{
    return false;
  }
}

bool Matrix::isSimilarToSymmetric()const{
  if(this->isSimilarTo(this->transpose())){
    return true;
  }else{
    return false;
  }
}

bool Matrix::isSimilarToSkewSymmetric()const{
  if(this->isSimilarTo(-(this->transpose()))){
    return true;
  }else{
    return false;
  }
}




double
Matrix::trace()const{
  if(_width!=_height){
    throw error::NotPermitted();
  }
  double tr= 0.0;
  for(Index i= 0;i!=_height;i++){
    tr+= _elements[i*_height+i];
  }
  return tr;
}




double
Matrix::det()const{
  if(_width!=_height){
    throw error::NotPermitted();
  }
  
  DBL*LU= new DBL[_size];
  this->copyValuesTo(LU);
  INT N= _height;
  INT INFO;
  INT*IPIV= new INT[_height];
  
  
  
  dgetrf_(&N,&N,LU,&N,IPIV,&INFO);
  
  
  
  
  ;
  
  
  
  double d= 0.0;
  if(INFO!=0){
    return d;
  }
  
  d= 1.0;
  for(Index i= 0;i!=_height;i++){
    if(IPIV[i]!=(i+1)){
      d*= -LU[i*_height+i];
    }else{
      d*= LU[i*_height+i];
    }
  }
  
  
  
  ;
  delete[]LU;
  return d;
}




Vector
Matrix::svd()const{
  Matrix U(_height,_height);
  Matrix V(_width,_width);
  return svd(U,V);
}

Vector
Matrix::svd(Matrix&U,Matrix&V)const{
  INT m= _height;
  INT n= _width;
  INT lda= m;
  INT ldu= m;
  INT ldvt= n;
  INT ds= _min(m,n);
  
  char jobz= 'A';
  INT lwork= 3*ds*ds+_max(_max(m,n),5*_min(m,n)*_min(m,n)+4*_min(m,n));
  DBL*el= new DBL[_size];
  this->copyValuesTo(el);
  DBL*s= new DBL[ds];
  DBL*u= new DBL[ldu*m];
  DBL*vt= new DBL[ldvt*n];
  DBL*work= new DBL[_max(1,lwork)];
  INT*iwork= new INT[8*ds];
  INT info;
  
  dgesdd_(&jobz,&m,&n,el,&lda,s,u,&ldu,vt,&ldvt,work,&lwork,
          iwork,&info);
  
  if(info> 0){
    throw error::FailedToConverge();
  }
  
  Vector S(ds,s);
  U= Matrix(m,m,u);
  V= Matrix(n,n,vt).transpose();
  
  return S;
}




Matrix Matrix::eig(JobSpec js)const{
  Matrix e(1,_width);
  
  
  
  
  INT N= _width;
  Matrix A(*this);
  INT LDA= N;
  INT INFO;
  
  if(this->isSymmetric()==true){
    
    
    
    char JOBZ;
    if(js==EIGENVALUE_ONLY){
      JOBZ= 'N';
    }else{
      JOBZ= 'V';
    }
    char UPLO= 'U';
    DBL*W= new DBL[N];
    INT LWORK= 3*N;
    DBL*WORK= new DBL[LWORK];
    
    dsyev_(&JOBZ,&UPLO,&N,A.elements(),&LDA,W,WORK,&LWORK,&INFO);
    
    if(INFO==0){
      for(Index i= 0;i!=N;i++){
        e(1,i+1)= W[i];
      }
      if(js!=EIGENVALUE_ONLY){
        e.appendRows(A);
      }
      delete[]W;
    }else{
      delete[]W;
      throw error::FailureInSolvingEigenvalues();
    }
    
    
    
    ;
  }else{
    
    
    
    Matrix VL,VR;
    char JOBVL,JOBVR;
    INT LDVL= N;
    INT LDVR= N;
    switch(js){
      case EIGENVALUE_ONLY:
        JOBVL= JOBVR= 'N';
        break;
        
      case EIGENVALUE_AND_EIGENVECTOR:
        JOBVL= JOBVR= 'V';
        VL= Matrix(LDVL,N);
        VR= Matrix(LDVR,N);
        break;
        
      case EIGENVALUE_AND_LEFT_EIGENVECTOR:
        JOBVL= 'V';
        JOBVR= 'N';
        VL= Matrix(LDVL,N);
        break;
        
      case EIGENVALUE_AND_RIGHT_EIGENVECTOR:
        JOBVL= 'N';
        JOBVR= 'V';
        VR= Matrix(LDVR,N);
        break;
    }
    DBL*WR= new DBL[N];
    DBL*WI= new DBL[N];
    
    INT LWORK= 4*N;
    DBL*WORK= new DBL[LWORK];
    
    dgeev_(&JOBVL,&JOBVR,&N,A.elements(),&LDA,WR,WI,VL.elements(),&LDVL,
           VR.elements(),&LDVR,WORK,&LWORK,&INFO);
    
    if(INFO==0){
      
      
      
      for(Index i= 0;i!=N;i++){
        e(1,i+1)= WR[i];
      }
      Vector imaginery= Vector(N,WI,ROW_VECTOR);
      e.appendRowVector(imaginery);
      
      switch(js){
        case EIGENVALUE_ONLY:
          break;
          
        case EIGENVALUE_AND_EIGENVECTOR:
          e.appendRows(VL);
          e.appendRows(VR);
          break;
          
        case EIGENVALUE_AND_LEFT_EIGENVECTOR:
          e.appendRows(VL);
          break;
          
        case EIGENVALUE_AND_RIGHT_EIGENVECTOR:
          e.appendRows(VR);
          break;
      }
      
      
      
      ;
      
      
      
      delete[]WR;
      delete[]WI;
      delete[]WORK;
      
      
      
      
      ;
    }else{
      
      
      
      delete[]WR;
      delete[]WI;
      delete[]WORK;
      
      
      
      
      ;
      throw error::FailureInSolvingEigenvalues();
    }
    
    
    
    ;
  }
  return e;
}




Matrix Matrix::eigenvalues()const{
  if(this->isSquare()==false){
    throw error::NonSquare();
  }
  
  return eig(EIGENVALUE_ONLY);
}

Matrix Matrix::eigenvectors()const{
  if(this->isSquare()==false){
    throw error::NonSquare();
  }
  
  return eig(EIGENVALUE_AND_RIGHT_EIGENVECTOR);
}

Matrix Matrix::leftEigenvectors()const{
  if(this->isSquare()==false){
    throw error::NonSquare();
  }
  
  return eig(EIGENVALUE_AND_LEFT_EIGENVECTOR);
}

Matrix Matrix::leftRightEigenvectors()const{
  if(this->isSquare()==false){
    throw error::NonSquare();
  }
  
  return eig(EIGENVALUE_AND_EIGENVECTOR);
}




Vector Matrix::realEigenvaluesForSymmetricPart()const{
  Vector e(_width);
  if(isSymmetric()==false){
    Matrix As= .5*(*this+(this->transpose()));
    e= As.eigenvalues().row(1);
  }else{
    e= eigenvalues().row(1);
  }
  return e;
}




bool Matrix::isPositiveDefinite()const{
  if(isSquare()==false){
    throw error::NonSquare();
  }
  
  Vector e= realEigenvaluesForSymmetricPart();
  
  bool positiveDefiniteness= true;
  for(Index i= 1;i!=(_width+1);i++){
    if(e(i)<=0.0){
      positiveDefiniteness= false;
      break;
    }
  }
  return positiveDefiniteness;
}

bool Matrix::isPositiveSemiDefinite()const{
  if(isSquare()==false){
    throw error::NonSquare();
  }
  
  Vector e= realEigenvaluesForSymmetricPart();
  
  bool positiveSemiDefiniteness= true;
  for(Index i= 1;i!=(_width+1);i++){
    if(e(i)<0.0){
      positiveSemiDefiniteness= false;
      break;
    }
  }
  
  return positiveSemiDefiniteness;
}





bool Matrix::isSimilarToPositiveSemiDefinite()const{
  if(isPositiveSemiDefinite()==true){
    return true;
  }
  
  if(isSquare()==false){
    throw error::NonSquare();
  }
  
  Vector e= realEigenvaluesForSymmetricPart();
  
  bool positiveSemiDefiniteness= true;
  for(Index i= 1;i!=(_width+1);i++){
    if(e(i)<-TOLERANCE){
      positiveSemiDefiniteness= false;
      break;
    }
  }
  
  return positiveSemiDefiniteness;
}







void
Matrix::printMatrixAsEigenvalueEigenvector(PrintFormat fmt,
                                           std::ostream&strm
                                           )const{
  int width= _set_print_format(fmt,strm);
  laft::Size n= _width;
  if((_height==1)||(_height==1+n)){
    
    
    
    strm<<std::endl<<"Real eigenvalues:"<<std::endl;
    for(Index j= 1;j!=n+1;j++){
      strm.width(width);
      strm<<"["<<(*this)(1,j)<<" ]"<<std::endl;
    }
    
    
    
    ;
  }else if((_height==2)||(_height==2+n)||(_height==2+2*n)){
    
    
    
    
    strm<<std::endl<<"Complex eigenvalues:"<<std::endl;
    for(Index j= 1;j!=n+1;j++){
      strm<<"[";
      strm.width(width);
      strm<<(*this)(1,j)<<" ";
      if((*this)(2,j)>=0){
        strm<<"+";
      }else{
        strm<<" ";
      }
      strm.width(width);
      strm<<(*this)(2,j)<<"i"<<" ]"<<std::endl;
    }
    
    
    
    ;
  }
  
  if(_height==1+n){
    
    
    
    strm<<std::endl<<"Real eigenvectors:"<<std::endl;
    for(Index i= 2;i!=n+2;i++){
      strm<<"[";
      for(Index j= 1;j!=n+1;j++){
        strm.width(width);
        strm<<(*this)(i,j);
      }
      strm<<" ]"<<std::endl;
    }
    
    
    
    ;
  }else if(_height==2+n){
    
    
    
    strm<<std::endl<<"Complex eigenvectors:"<<std::endl;
    for(Index j= 1;j!=n+1;j++){
      if((*this)(2,j)==0.0){
        strm<<"Eigenvector "<<j<<" is real."<<std::endl;
        for(Index i= 3;i!=3+n;i++){
          strm<<"[";
          strm.width(width);
          strm<<(*this)(i,j)<<" ]"<<std::endl;
        }
        strm<<std::endl;
      }else{
        
        strm<<"Eigenvectors "<<j<<" and "<<j+1<<" are complex conjugate."
        <<std::endl;
        printComplexConjugateEigenvectors(3,j,n,width,strm);
        j++;
      }
    }
    
    
    
    ;
  }else if(_height==2+2*n){
    
    
    
    strm<<std::endl<<"Complex left eigenvectors:"<<std::endl;
    for(Index j= 1;j!=n+1;j++){
      if((*this)(2,j)==0.0){
        strm<<"Eigenvector "<<j<<" is real."<<std::endl;
        for(Index i= 3;i!=3+n;i++){
          strm<<"[";
          strm.width(width);
          strm<<(*this)(i,j)<<" ]"<<std::endl;
        }
        strm<<std::endl;
      }else{
        
        strm<<"Eigenvectors "<<j<<" and "<<j+1<<" are complex conjugate."
        <<std::endl;
        printComplexConjugateEigenvectors(3,j,n,width,strm);
        j++;
      }
    }
    
    strm<<std::endl<<"Complex right eigenvectors:"<<std::endl;
    for(Index j= 1;j!=n+1;j++){
      if((*this)(2,j)==0.0){
        strm<<"Eigenvector "<<j<<" is real."<<std::endl;
        for(Index i= 3+n;i!=3+2*n;i++){
          strm<<"[";
          strm.width(width);
          strm<<(*this)(i,j)<<" ]"<<std::endl;
        }
        strm<<std::endl;
      }else{
        
        strm<<"Eigenvectors "<<j<<" and "<<j+1<<" are complex conjugate."
        <<std::endl;
        printComplexConjugateEigenvectors(3+n,j,n,width,strm);
        j++;
      }
    }
    
    
    
    ;
  }
  _restore_print_format(strm);
}




void Matrix::print(PrintFormat fmt,std::ostream&strm)const{
  
  
  
  strm<<std::endl;
  if(_size==0){
    strm<<" Empty matrix"<<std::endl;
  }else{
    strm<<" "<<_height<<" x "<<_width<<" matrix"<<std::endl;
  }
  char prev= strm.fill('-');
  strm<<std::setw(80)<<'-'<<std::endl;
  strm.fill(prev);
  
  
  
  
  
  
  
  ;
  int width= _set_print_format(fmt,strm);
  
  
  
  for(Index i= 0;i!=_height;i++){
    strm<<"[";
    for(Index j= 0;j!=_width;j++){
      strm.width(width);
      strm<<_elements[j*_height+i];
    }
    strm<<" ]"<<std::endl;
  }
  strm<<std::endl<<std::endl;
  
  
  
  
  
  ;
  _restore_print_format(strm);
}







void
Matrix::recount(){
  _size= _width*_height;
}




void Matrix::_format(const char*fmt){
  
  
  
  bool endOfLine= false;
  bool rowEnded= false;
  bool numericRepPreceded= false;
  char ch;
  laft::Size m,n;
  unsigned i,j,idx;
  
  m= n= 0;
  idx= i= j= 0;
  while(!endOfLine){
    ch= fmt[idx++];
    if(ch=='\t'||ch=='\r'||ch=='\n'||ch==' '||
       ch==','||ch==';'||ch=='\0'){
      
      
      
      if(numericRepPreceded==true){
        j++;
        numericRepPreceded= false;
      }
      
      
      
      
      ;
      
      
      
      if(ch==';'||ch=='\0'){
        rowEnded= true;
        if(i==0){
          n= j;
        }else{
          if(n!=j)throw error::IllegalConstruction();
        }
      }
      
      
      
      
      
      ;
      
      
      
      if(ch=='\0'){
        m= ++i;
        endOfLine= true;
      }
      
      
      
      
      
      ;
    }else{
      
      
      
      if(rowEnded==true){
        i++;
        j= 0;
        rowEnded= false;
      }
      numericRepPreceded= true;
      
      
      
      
      
      
      
      ;
    }
  }
  _height= m;_width= n;
  _size= m*n;
  
  
  
  
  
  ;
  
  
  
  if(_size> 0){
    _elements= new DBL[_size];
    for(Index i= 0;i!=_size;i++)_elements[i]= 0.0;
  }else _elements= NULL;
  
  unsigned ddx;
  char buf[128];
  endOfLine= false;
  idx= ddx= i= j= 0;
  while(!endOfLine){
    ch= fmt[idx++];
    if(ch=='\t'||ch=='\r'||ch=='\n'
       ||ch==' '||ch==','||ch==';'||ch=='\0'){
      if(ddx> 0){
        buf[ddx]= '\0';
        _elements[j*m+i]= atof(buf);
        j++;
        ddx= 0;
      }
      if(ch==';'||ch=='\0'){i++;j= 0;}
      if(ch=='\0')endOfLine= true;
    }else buf[ddx++]= ch;
  }
  
  
  
  ;
}




bool
Matrix::doesIndicesExceedMatrixDimension(const Index&i,const Index&j)const{
  if((i<1)||(_height<i)||(j<1)||(_width<j)){
    return true;
  }else{
    return false;
  }
}





bool
Matrix::hasSameDimensionsWith(const Matrix&m)const{
  if((_width==m._width)&&(_height==m._height)){
    return true;
  }else{
    return false;
  }
}





void
Matrix::_replace_rows_with_matrix(
                                  DBL*el,const laft::Size h,const laft::Size w,const Index p,
                                  const laft::Size n,const Matrix&m,const Index q){
  
  if(p<1||h<(p+n-1)||q<1||m.height()<(q+n-1)){
    throw error::NotPermitted();
  }
  
  for(Index i= 0;i!=n;i++){
    for(Index j= 0;j!=w;j++){
      el[j*h+i+p-1]= m._elements[j*m._height+i+q-1];
    }
  }
}




bool
Matrix::doesIndexExceedMatrixRowDimension(const Index i)const{
  if((i<1)||(_height<i)){
    return true;
  }else{
    return false;
  }
}




void
Matrix::_replace_columns_with_matrix(
                                     DBL*el,const laft::Size h,const laft::Size w,const Index p,
                                     const laft::Size n,const Matrix&m,const Index q){
  if(p<1||w<(p+n-1)||q<1||m.width()<(q+n-1)){
    throw error::NotPermitted();
  }
  Index offset1= h*(p-1);
  Index offset2= h*(q-1);
  for(Index k= 0;k!=h*n;k++){
    el[offset1+k]= m._elements[offset2+k];
  }
}




bool
Matrix::doesIndexExceedMatrixColumnDimension(const Index j)const{
  if((j<1)||(_width<j)){
    return true;
  }else{
    return false;
  }
}




double
Matrix::_prod_diagonal()const{
  double d= 1.0;
  for(Index i= 0;i!=_height;i++){
    d*= _elements[i*_height+i];
  }
  return d;
}




void
Matrix::printComplexConjugateEigenvectors(Index row,Index col,laft::Size len,
                                          int width,
                                          std::ostream&strm)const{
  for(Index i= row;i!=row+len;i++){
    char sgn;
    strm<<"[";
    strm.width(width);
    strm<<(*this)(i,col)<<" ";
    if((*this)(i,col+1)>=0){
      strm<<"+";sgn= ' ';
    }else{
      strm<<" ";sgn= '+';
    }
    strm.width(width);
    strm<<(*this)(i,col+1)<<"i"<<" ] [";
    
    strm.width(width);
    strm<<(*this)(i,col)<<" "<<sgn;
    
    strm.width(width);
    strm<<-(*this)(i,col+1)<<"i"<<" ]"<<std::endl;
  }
}







Matrix
laft::operator+(const double s,const Matrix&m){
  return m+s;
}

Matrix
laft::operator-(const double s,const Matrix&m){
  return-m+s;
}




Matrix laft::operator*(const double&s,const Matrix&m){
  return m*s;
}




Vector
laft::diag(const Matrix&m){
  return m.diag();
}




Matrix
laft::transpose(const Matrix&m){
  return m.transpose();
}




Matrix
laft::inv(const Matrix&m){
  return m.inv();
}




bool laft::isSquare(const Matrix&m){
  return m.isSquare();
}

bool laft::isSymmetric(const Matrix&m){
  return m.isSymmetric();
}

bool laft::isSkewSymmetric(const Matrix&m){
  return m.isSkewSymmetric();
}

bool laft::isSimilarToSymmetric(const Matrix&m){
  return m.isSimilarToSymmetric();
}

bool laft::isSimilarToSkewSymmetric(const Matrix&m){
  return m.isSimilarToSkewSymmetric();
}




double
laft::trace(const Matrix&m){
  return m.trace();
}




double
laft::det(const Matrix&m){
  return m.det();
}




Vector
laft::svd(const Matrix&m){
  return m.svd();
}




Matrix
laft::eigenvalues(const Matrix&m){
  return m.eigenvalues();
}

Matrix
laft::eigenvectors(const Matrix&m){
  return m.eigenvectors();
}

Matrix
laft::leftEigenvectors(const Matrix&m){
  return m.leftEigenvectors();
}

Matrix
laft::leftRightEigenvectors(const Matrix&m){
  return m.leftRightEigenvectors();
}




void laft::printMatrixAsEigenvalueEigenvector(const Matrix&evm,
                                              PrintFormat fmt,
                                              std::ostream&strm){
  evm.printMatrixAsEigenvalueEigenvector(fmt,strm);
}




bool laft::isPositiveDefinite(const Matrix&m){
  return m.isPositiveDefinite();
}

bool laft::isPositiveSemiDefinite(const Matrix&m){
  return m.isPositiveSemiDefinite();
}

bool laft::isSimilarToPositiveSemiDefinite(const Matrix&m){
  return m.isSimilarToPositiveSemiDefinite();
}




Matrix
Matrix::identity(const laft::Size m,const laft::Size n){
  Matrix mat(m,n);
  const laft::Size smaller= (m> n)?n:m;
  DBL*el= mat.elements();
  
  for(Index i= 0;i!=smaller;i++){
    el[m*i+i]= 1.0;
  }
  
  return mat;
}




Matrix
Matrix::diag(const Vector&v,const int k){
  Index offset= abs(k);
  laft::Size sz= v.dim()+offset;
  Matrix m(sz,sz);
  DBL*mel= m.elements();
  DBL*vel= v.elements();
  if(k> 0){
    for(Index i= 0;i!=v.dim();i++){
      mel[(i+offset)*sz+i]= vel[i];
    }
  }else if(k<0){
    for(Index i= 0;i!=v.dim();i++){
      mel[i*sz+offset+i]= vel[i];
    }
  }else{
    for(Index i= 0;i!=v.dim();i++){
      mel[i*sz+i]= vel[i];
    }
  }
  return m;
}















