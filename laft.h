


#ifndef __LINEAR_ALGEBRA_FOUNDATION_TOOLKIT_H_
#define __LINEAR_ALGEBRA_FOUNDATION_TOOLKIT_H_

#include <fstream>
#include <cmath>
#include <stdlib.h>

#if __cplusplus >= 201103L
#include <random>
#endif

#if defined(__APPLE__)
#include <Accelerate/Accelerate.h>
typedef __CLPK_integer INT;
typedef __CLPK_doublereal DBL;
#endif

#if defined(_WIN32) || defined(_WIN64)
#include <ctime>
#include <cstdint>
#include "f2c.h"
#include "clapack.h"
typedef integer INT;
typedef doublereal DBL;
#endif

#if defined(__linux) || defined(__linux__)
#include "f2c.h"
#include "clapack.h"
typedef integer INT;
typedef doublereal DBL;
#endif




#include <iostream>
#include <iomanip>
#include <string>







#include <limits>




namespace laft{
  
  
  
  typedef uint32_t Index;
  typedef uint32_t Size;
  class Array;
  class Vector;
  class Matrix;
  
  
  
  
  
  
  
  
  
  const DBL EPS= 2.2204e-16;
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  enum ToleranceLevel{
    TL_ZERO= 0,
    TL_ONE= 1,TL_TWO= 2,TL_THREE= 3,TL_FOUR= 4,TL_FIVE= 5,
    TL_SIX= 6,TL_SEVEN= 7,TL_EIGHT= 8,TL_NINE= 9,TL_TEN= 10
  };
  
  
  
  
  enum InitialValues{
    ZEROS,
    ONES,
    RAND,
    RANDN
  };
  
  
  
  
  enum PrintFormat{
    SHORT,
    NORMAL,
    EXTENDED,
    SCIENTIFIC,
    EXTSCIENTIFIC
  };
  
  
  
  
  
  
  enum VectorType{
    ROW_VECTOR,
    COLUMN_VECTOR
  };
  
  
  
  
  
  
  const unsigned Inf=
#if defined(__linux__) || defined(__linux)
  100000000;
#else
#if defined(_WIN32) || defined(_WIN64)
#ifdef max
#undef max
#endif
#endif
  std::numeric_limits<unsigned> ::max();
#endif
  
  
  
  
  
  
  enum JobSpec{
    EIGENVALUE_ONLY,
    EIGENVALUE_AND_EIGENVECTOR,
    EIGENVALUE_AND_LEFT_EIGENVECTOR,
    EIGENVALUE_AND_RIGHT_EIGENVECTOR
  };
  
  
  
  
  
  
  
  void _copy_elements(laft::Size,DBL*,DBL*);
  
  
  
  
  
  void _add_elements(laft::Size,DBL*,DBL*,DBL*);
  void _subtract_elements(laft::Size,DBL*,DBL*,DBL*);
  
  
  
  
  
  
  void _negate_elements(laft::Size,DBL*,DBL*);
  
  
  
  
  
  
  
  void _multiply_scalar_to_elements(laft::Size,DBL*,const DBL&,DBL*);
  void _divide_elements_by_scalar(laft::Size,DBL*,const DBL&,DBL*);
  
  
  
  
  
  bool _exclusiveOR(const bool&,const bool&);
  
  
  
  
  
  
  
  laft::Size _min(const laft::Size&,const laft::Size&);
  laft::Size _max(const laft::Size&,const laft::Size&);
  
  
  
  
  
  
  
  void _generate_uniform_random_numbers(DBL*,const laft::Size&);
  void _generate_normal_random_numbers(DBL*,const laft::Size&);
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  Index indexOfMaximumAbsoluteElement(laft::Size,double*);
  Index indexOfMinimumAbsoluteElement(laft::Size,double*);
  Index indexOfMaximumElement(laft::Size,double*);
  Index indexOfMinimumElement(laft::Size,double*);
  
  
  
  
  
  
  Matrix diag(const Vector&,const int k= 0);
  
  
  
  
  
  
  
  
  namespace error{
    class Generic{
    protected:
      std::string theMessage;
      
    public:
      
      
      
      Generic(void);
      Generic(const char*msg);
      const std::string&message(void)const;
      
      
      
      
      
      
      
      virtual~Generic();
    };
    
    
    
    class NotPermitted:public Generic{
    public:
      NotPermitted();
    };
    
    class Singular:public Generic{
    public:
      Singular();
    };
    
    class FileError:public Generic{
    public:
      FileError();
    };
    
    class NonSquare:public Generic{
    public:
      NonSquare();
    };
    
    class NonPositiveDefinite:public Generic{
    public:
      NonPositiveDefinite();
    };
    
    class NonPositiveSemiDefinite:public Generic{
    public:
      NonPositiveSemiDefinite();
    };
    
    class NotImplemented:public Generic{
    public:
      NotImplemented();
    };
    
    class RankDefficient:public Generic{
    public:
      RankDefficient();
    };
    
    class CplusplusElevenRequired:public Generic{
    public:
      CplusplusElevenRequired();
    };
    
    
    
    
    
    
    class DivisionByZero:public Generic{
    public:
      DivisionByZero();
    };
    
    
    
    
    class IllegalConstruction:public Generic{
    public:
      IllegalConstruction();
    };
    
    
    
    
    class OutOfRangeIndex:public Generic{
    public:
      OutOfRangeIndex();
    };
    
    
    
    
    class DimensionMismatch:public Generic{
    public:
      DimensionMismatch();
    };
    
    
    
    
    class FailedToConverge:public Generic{
    public:
      FailedToConverge();
    };
    
    
    
    
    class FailureInSolvingEigenvalues:public Generic{
    public:
      FailureInSolvingEigenvalues();
    };
    
    
    
    
  }
  
  
  
  
  
  
  
  
  
  
  class Array{
  protected:
    laft::Size _size;
    DBL*_elements;
    DBL TOLERANCE;
    
  public:
    
    
    
  public:
    virtual void setToleranceLevel(ToleranceLevel);
    virtual ToleranceLevel toleranceLevel()const;
    virtual DBL tolerance()const;
    virtual void increaseTolerance();
    virtual void decreaseTolerance();
    virtual void initTolerance();
    
    
    
    
    
  public:
    virtual laft::Size size()const;
    virtual laft::Size count()const;
    virtual void setSize(laft::Size);
    virtual DBL*elements()const;
    virtual void setElements(DBL*);
    
    
    
    
    
    
    
  public:
    virtual void copyValuesTo(double*)const;
    
    
    
    
    
    
    
    
    
    
  protected:
    Array();
    Array(const laft::Size theSize);
    
    
    
    
    
  protected:
    Array(laft::Size,InitialValues);
    
    
    
    
    
    
    
    
  protected:
    Array(laft::Size,double*);
    
    
    
    
    
    
    
  protected:
    Array(const Array&src);
    
    
    
    
    
    virtual~Array();
    
    
    
    
    
    
    
    
    
    
    
    
    
  public:
    virtual bool operator==(const Array&)const;
    virtual bool operator!=(const Array&)const;
    virtual bool isSimilarTo(const Array&)const;
    
    
    
    
    
    
    
    
  public:
    virtual void print(PrintFormat fmt= NORMAL,std::ostream&strm= std::cout)const;
    
    
    
    
    
    
  protected:
    virtual int _set_print_format(PrintFormat,std::ostream&)const;
    
    
    
    
    
  protected:
    virtual void _restore_print_format(std::ostream&)const;
    
    
    
    
    
    
    
    
    
    
    
  };
  
  
  
  std::ostream&operator<<(std::ostream&,const Array&);
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  class Vector:public Array{
  private:
    VectorType _type;
    
    
    
    
  public:
    virtual VectorType type()const;
    virtual void setType(VectorType);
    virtual bool isRowVector()const;
    virtual bool isColumnVector()const;
    virtual laft::Size dim()const;
    virtual laft::Size dimension()const;
    
    
    
    
    
    
    
    
    
    
    
    
    
  public:
    Vector();
    
    
    
    
  public:
    Vector(laft::Size theSize,VectorType type= COLUMN_VECTOR);
    
    
    
    
  public:
    Vector(laft::Size,InitialValues,VectorType type= COLUMN_VECTOR);
    
    
    
    
    
    
    
  public:
    Vector(laft::Size,double*,VectorType type= COLUMN_VECTOR);
    
    
    
    
    
    
  public:
    Vector(const Vector&);
    
    
    
    
    
    
  public:
    Vector(const char*);
    
    
    
    
  protected:
    virtual void _format(const char*);
    
    
    
    
    
  public:
    ~Vector();
    
    
    
    
    
    
    
    
    
  public:
    virtual const DBL&operator()(const Index&)const;
    virtual DBL&operator()(const Index&);
    virtual bool doesIndexExceedVectorDimension(const Index&)const;
    
    
    
    
    
    
  public:
    virtual Vector&operator= (const Vector&);
    
    
    
    
    
  public:
    virtual bool operator==(const Vector&)const;
    virtual bool operator!=(const Vector&)const;
    virtual bool isSimilarTo(const Vector&)const;
    
    
    
    
    
  public:
    virtual Vector operator+(const Vector&)const;
    virtual Vector operator-(const Vector&)const;
    virtual Vector&operator+= (const Vector&);
    virtual Vector&operator-= (const Vector&);
    
    
    
    
    
    
    
  public:
    virtual Vector operator+(const double)const;
    virtual Vector operator-(const double)const;
    virtual Vector&operator+= (const double);
    virtual Vector&operator-= (const double);
    
    
    
    
  public:
    virtual Vector operator-()const;
    
    
    
    
    
  public:
    virtual Vector operator*(const double&)const;
    virtual Vector operator/(const double&)const;
    virtual Vector&operator*= (const double&);
    virtual Vector&operator/= (const double&);
    
    
    
    
  public:
    Vector operator*(const Matrix&)const;
    
    
    
    
    
    
    
  public:
    virtual void insertElementAt(const double&,const Index);
    
    
    
    
    
  public:
    virtual void appendElement(const double&);
    
    
    
    
    
    
  public:
    virtual void insertVectorAt(const Vector&,const Index);
    
    
    
    
    
    
  public:
    virtual void appendVector(const Vector&);
    
    
    
    
    
    
  public:
    virtual void removeElementAt(const Index);
    
    
    
    
    
  public:
    virtual void removeElementsInRange(const Index,const Index);
    
    
    
    
    
    
    
  public:
    virtual double norm(const unsigned p= 2)const;
    
    
    
    
    
  public:
    virtual Vector unit(const unsigned p= 2)const;
    virtual Vector normalize(const unsigned p= 2);
    
    
    
    
    
  public:
    virtual Vector transpose()const;
    virtual Vector T()const;
    
    
    
    
    
  public:
    virtual double operator*(const Vector&)const;
    
    
    
    
    
    
    
  public:
    virtual void print(PrintFormat= NORMAL,std::ostream&strm= std::cout)const;
    
    
    
    
  };
  
  
  
  Vector operator+(const double,const Vector&);
  Vector operator-(const double,const Vector&);
  
  
  
  
  
  
  
  
  
  
  Vector operator*(const double&,const Vector&);
  
  
  
  
  
  
  double norm(const Vector&v,const unsigned p= 2);
  Vector unit(const Vector&v,const unsigned p= 2);
  Vector normalize(Vector&v,const unsigned p= 2);
  
  
  
  
  
  Vector transpose(const Vector&);
  
  
  
  
  
  
  
  double dot(const Vector&,const Vector&);
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  class Matrix:public Array{
  protected:
    laft::Size _width;
    laft::Size _height;
    
    
    
    
  public:
    virtual laft::Size width()const;
    virtual laft::Size numberOfColumns()const;
    virtual laft::Size numCols()const;
    virtual laft::Size height()const;
    virtual laft::Size numberOfRows()const;
    virtual laft::Size numRows()const;
    virtual void setDimensions(laft::Size,laft::Size);
    
    
    
    
    
    virtual void recount();
    
    
    
    
    
    
    
    
    
    
  public:
    Matrix();
    Matrix(const laft::Size,const laft::Size);
    
    
    
    
    
  public:
    Matrix(const laft::Size,const laft::Size,const InitialValues);
    
    
    
    
    
  public:
    Matrix(const laft::Size,const laft::Size,double*);
    
    
    
    
    
    
    
  public:
    Matrix(const Matrix&);
    
    
    
    
    
  public:
    Matrix(const char*);
    
    
    
    
    
  protected:
    virtual void _format(const char*);
    
    
    
    
    
    
    
    
  public:
    virtual const DBL&operator()(const Index&,const Index&)const;
    virtual DBL&operator()(const Index&,const Index&);
    virtual bool doesIndicesExceedMatrixDimension(const Index&,const Index&)const;
    
    
    
    
    
    
  public:
    virtual Matrix&operator= (const Matrix&);
    virtual bool hasSameDimensionsWith(const Matrix&)const;
    virtual bool operator==(const Matrix&)const;
    virtual bool operator!=(const Matrix&)const;
    virtual bool isSimilarTo(const Matrix&)const;
    
    
    
    
    
    
    
  public:
    virtual Matrix operator+(const Matrix&)const;
    virtual Matrix operator-(const Matrix&)const;
    virtual Matrix&operator+= (const Matrix&);
    virtual Matrix&operator-= (const Matrix&);
    virtual Matrix operator+(const double)const;
    virtual Matrix operator-(const double)const;
    virtual Matrix&operator+= (const double);
    virtual Matrix&operator-= (const double);
    
    
    
    
    
  public:
    virtual Matrix operator-()const;
    
    
    
    
    
    
  public:
    virtual Matrix operator*(const double&)const;
    virtual Matrix operator*(const Matrix&)const;
    virtual Vector operator*(const Vector&)const;
    virtual Matrix operator/(const double&)const;
    virtual Matrix&operator*= (const double&);
    virtual Matrix&operator*= (const Matrix&);
    
    
    
    
    
    
    
    
    
    
    
  protected:
    virtual void _replace_rows_with_matrix(
                                           DBL*,const laft::Size,const laft::Size,const Index,
                                           const laft::Size,const Matrix&,const Index);
    
    
    
    
    
    
  public:
    virtual bool doesIndexExceedMatrixRowDimension(const Index)const;
    
    
    
    
    
  public:
    virtual void insertRowVectorAt(const Vector&,const Index);
    virtual void insertRowsAt(const Matrix&,const Index);
    virtual void appendRowVector(const Vector&);
    virtual void appendRows(const Matrix&);
    virtual void deleteRowAt(const Index);
    virtual void swapRows(const Index,const Index);
    
    
    
    
    
    
    
    
    
    
  protected:
    virtual void _replace_columns_with_matrix(
                                              DBL*,const laft::Size,const laft::Size,const Index,
                                              const laft::Size,const Matrix&,const Index);
    
    
    
    
    
    
  public:
    virtual bool doesIndexExceedMatrixColumnDimension(const Index)const;
    
    
    
    
  public:
    virtual void insertColumnVectorAt(const Vector&,const Index);
    virtual void insertColumnsAt(const Matrix&,const Index);
    virtual void appendColumnVector(const Vector&);
    virtual void appendColumns(const Matrix&);
    virtual void deleteColumnAt(const Index);
    virtual void swapColumns(const Index,const Index);
    
    
    
    
    
    
  public:
    virtual Vector row(const Index)const;
    virtual Vector column(const Index)const;
    
    
    
    
    
    
    virtual Vector diag()const;
    
    
    
    
    
  public:
    virtual Matrix
    subMatrix(const Index,const Index,const Index,const Index)const;
    
    
    
    
    
    
  public:
    virtual Matrix rows(const Index,const Index)const;
    virtual Matrix columns(const Index,const Index)const;
    
    
    
    
    
    
    
    
    
    
    
    
  public:
    virtual Matrix transpose()const;
    virtual Matrix T()const;
    
    
    
    
    
  public:
    virtual Matrix inv()const;
    
  protected:
    virtual double _prod_diagonal()const;
    
    
    
    
  public:
    bool isSquare()const;
    bool isSymmetric()const;
    bool isSkewSymmetric()const;
    bool isSimilarToSymmetric()const;
    bool isSimilarToSkewSymmetric()const;
    
    
    
    
    
  public:
    virtual double trace()const;
    
    
    
    
  public:
    virtual double det()const;
    
    
    
    
  public:
    virtual Vector svd()const;
    virtual Vector svd(Matrix&,Matrix&)const;
    
    
    
    
  protected:
    Matrix eig(JobSpec)const;
    
  public:
    Matrix eigenvalues()const;
    Matrix eigenvectors()const;
    Matrix leftEigenvectors()const;
    Matrix leftRightEigenvectors()const;
    
    
    
    
  public:
    virtual void
    printMatrixAsEigenvalueEigenvector(PrintFormat fmt= NORMAL,
                                       std::ostream&strm= std::cout)const;
  protected:
    virtual void
    printComplexConjugateEigenvectors(Index,Index,laft::Size,
                                      int,std::ostream&)const;
    
    
    
    
    
  public:
    Vector realEigenvaluesForSymmetricPart()const;
    bool isPositiveDefinite()const;
    bool isPositiveSemiDefinite()const;
    bool isSimilarToPositiveSemiDefinite()const;
    
    
    
    
    
  public:
    virtual void print(PrintFormat fmt= NORMAL,std::ostream&strm= std::cout)const;
    
    
    
    
    
  public:
    static Matrix identity(const laft::Size,const laft::Size);
    
    
    
    
    
    
  public:
    static Matrix diag(const Vector&,const int k= 0);
    
    
    
    
    
    
  };
  
  
  
  Matrix operator+(const double,const Matrix&);
  Matrix operator-(const double,const Matrix&);
  
  
  
  
  
  
  Matrix operator*(const double&,const Matrix&);
  
  
  
  
  
  Vector diag(const Matrix&);
  
  
  
  
  
  
  Matrix transpose(const Matrix&);
  
  
  
  
  
  Matrix inv(const Matrix&);
  
  
  
  
  
  
  
  
  
  bool isSquare(const Matrix&);
  bool isSymmetric(const Matrix&);
  bool isSkewSymmetric(const Matrix&);
  bool isSimilarToSymmetric(const Matrix&);
  bool isSimilarToSkewSymmetric(const Matrix&);
  
  
  
  
  
  
  double trace(const Matrix&);
  
  
  
  
  
  double det(const Matrix&);
  
  
  
  
  
  Vector svd(const Matrix&);
  
  
  
  
  
  
  Matrix eigenvalues(const Matrix&);
  Matrix eigenvectors(const Matrix&);
  Matrix leftEigenvectors(const Matrix&);
  Matrix leftRightEigenvectors(const Matrix&);
  
  
  
  
  
  void
  printMatrixAsEigenvalueEigenvector(const Matrix&evm,
                                     PrintFormat fmt= NORMAL,
                                     std::ostream&strm= std::cout);
  
  
  
  
  
  bool isPositiveDefinite(const Matrix&);
  bool isPositiveSemiDefinite(const Matrix&);
  bool isSimilarToPositiveSemiDefinite(const Matrix&);
  
  
  
  
  
  
  
  
  
  Matrix eye(const laft::Size);
  Matrix eye(const laft::Size,const laft::Size);
  Matrix zeros(const laft::Size);
  Matrix zeros(const laft::Size,const laft::Size);
  Matrix ones(const laft::Size);
  Matrix ones(const laft::Size,const laft::Size);
  
  
  
  
  
  
  
  
  
  
  
  
}
#endif







